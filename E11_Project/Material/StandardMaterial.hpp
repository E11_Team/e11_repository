#pragma once

#include "SuperMaterial.hpp"

#include <string>

namespace E11 {
    namespace Component {
        class Model;
    }
    namespace Material {
        class StandardMaterial : public SuperMaterial {
        public:
            StandardMaterial(std::string name);
            ~StandardMaterial();
        };
    }
}