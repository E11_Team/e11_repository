#pragma once

#include <vector>
#include <string>

namespace E11 {
    namespace Component {
        class Model;
    }
    namespace Material {
        class SuperMaterial {
        public:
            SuperMaterial(std::string name);
            virtual ~SuperMaterial();
            void AddModel(Component::Model* model);
            void RemoveModel(Component::Model* model);
            std::vector<Component::Model*>& GetModels();
            std::string GetName();
        private:
            std::vector<Component::Model*> mModels;
            std::string mName;
        };
    }
}
