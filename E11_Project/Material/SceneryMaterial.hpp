#pragma once

#include "SuperMaterial.hpp"

#include <string>

namespace E11 {
    namespace Material {
        class SceneryMaterial : public SuperMaterial {
        public:
            SceneryMaterial(std::string name);
            ~SceneryMaterial();
        };
    }
}