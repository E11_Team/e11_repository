#include "SuperMaterial.hpp"

#include <algorithm>

using namespace E11;
using namespace Material;

SuperMaterial::SuperMaterial(std::string name) {
    mName = name;
}

SuperMaterial::~SuperMaterial() {
    mModels.clear();
}

void SuperMaterial::AddModel(Component::Model* model) {
    mModels.push_back(model);
}

void SuperMaterial::RemoveModel(Component::Model* model) {
    mModels.erase(std::remove(mModels.begin(), mModels.end(), model), mModels.end());
}

std::vector<Component::Model*>& SuperMaterial::GetModels() {
    return mModels;
}

std::string SuperMaterial::GetName() {
    return mName;
}
