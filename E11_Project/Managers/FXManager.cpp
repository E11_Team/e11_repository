#include "FXManager.hpp"

#include <d3d11.h>

#include "../Core.hpp"
#include "../Window.hpp"
#include "ResourceManager.hpp"
#include "../Shaders/ComputeShader.hpp"
#include "../Image/Image.hpp"
#include "../Texture/Texture2D.hpp"
#include "../ShaderProgram/Copy.hpp"
#include "../Component/Lens.hpp"

using namespace E11;

FXManager::FXManager() {
    copy = new ShaderProgram::Copy(L"Resources/Shaders/Copy_VS.hlsl", L"Resources/Shaders/Copy_PS.hlsl");
}

FXManager::~FXManager() {
    delete copy;
}

void FXManager::CopyPS(Image* srcImage, Image* trgImage) {
    copy->Bind();
    copy->Dispatch(srcImage->srv, trgImage->rtv);
    copy->UnBind();
}

void FXManager::CopyPS(ID3D11ShaderResourceView* srcSRV, ID3D11RenderTargetView* trgRTV) {
    copy->Bind();
    copy->Dispatch(srcSRV, trgRTV);
    copy->UnBind();
}

void FXManager::CopyCPU(Image* srcImage, Image* trgImage) {
    ID3D11Resource* srcResource;
    srcImage->srv->GetResource(&srcResource);
    ID3D11Resource* trgResource;
    trgImage->rtv->GetResource(&trgResource);
    Core().deviceContext->CopyResource(trgResource, srcResource);
    srcResource->Release();
    trgResource->Release();
}

void FXManager::CopyCS(Image* srcImage, Image* trgImage) {
    Core().resourceManager->copyCS->Dispatch(trgImage->GetWidth()/32 + 1, trgImage->GetHeight() / 32 + 1, srcImage->srv, trgImage->uav);
}

void FXManager::Refraction(Image* srcImage, Image* trgImage, DirectX::SimpleMath::Vector2 uvOffset, Component::Lens* lens) {
    Core().resourceManager->refractionConstData.uvOffset = uvOffset;
    Core().resourceManager->refractionConstData.textureDim.x = static_cast<float>(Core().resourceManager->refraction->textures[0]->GetWidth());
    Core().resourceManager->refractionConstData.textureDim.y = static_cast<float>(Core().resourceManager->refraction->textures[0]->GetHeight());
    Core().resourceManager->refractionConstData.targetDim.x = static_cast<float>(trgImage->GetWidth());
    Core().resourceManager->refractionConstData.targetDim.y = static_cast<float>(trgImage->GetHeight());
    Core().resourceManager->refractionConstData.nearZ = lens->nearZ;
    Core().resourceManager->refractionConstData.farZ = lens->farZ;
    Core().resourceManager->refractionConstData.fov = lens->fov;
    Core().deviceContext->CSSetShaderResources(2, 1, &Core().resourceManager->refractionImage->srv);
    Core().deviceContext->CSSetShaderResources(3, 1, &srcImage->worldPosSrv);
    Core().resourceManager->refraction->Dispatch(trgImage->GetWidth() / 32 + 1, trgImage->GetHeight() / 32 + 1, srcImage->srv, trgImage->uav);
    ID3D11ShaderResourceView *pSRV[2] = { NULL , NULL };
    Core().deviceContext->CSSetShaderResources(2, 2, pSRV);
}
