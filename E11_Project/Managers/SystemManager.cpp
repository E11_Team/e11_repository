#include "SystemManager.hpp"

#include "../System/RenderSystem.hpp"
#include "../System/PhysicsSystem.hpp"
#include "../System/TransformSystem.hpp"
#include "../System/ParticleSystem.hpp"
#include "../System/UpdateSystem.hpp"
#include "../System/PickingSystem.hpp"

using namespace E11;
using namespace System;

SystemManager::SystemManager() {
    renderSystem = new RenderSystem();
    physicsSystem = new PhysicsSystem();
    transformSystem = new TransformSystem();
    particleSystem = new ParticleSystem();
    updateSystem = new UpdateSystem();
    pickingSystem = new PickingSystem();
}

SystemManager::~SystemManager() {
    delete renderSystem;
    delete physicsSystem;
    delete transformSystem;
    delete particleSystem;
    delete updateSystem;
    delete pickingSystem;
}
