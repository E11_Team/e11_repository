#pragma once

#include <map>
#include <string>
#include <vector>
#include <typeinfo>
#include <algorithm>

#include <SimpleMath.h>

namespace E11 {
    namespace Geometry {
        class SuperGeometry;
        class Plane;
        class OBJMesh;
    }
    namespace Texture {
        class SuperTexture;
        class Texture2D;
    }
    namespace Material {
        class SuperMaterial;
        class StandardMaterial;
    }
    class Image;
    class ComputeShader;
    class ResourceManager {
    public:
        ResourceManager();
        ~ResourceManager();

        Geometry::Plane* CreatePlane();
        Geometry::OBJMesh* CreateOBJMesh(std::string filePath);
        Texture::Texture2D* Create2DTexture(std::wstring filePath);
        Image* CreateImage(unsigned int width, unsigned int height);

        template <typename T> T* CreateMaterial(std::string name);
        template <typename T> unsigned int GetMaterialSize();
        template <typename T> T* GetMaterialAt(const unsigned int i);

        ComputeShader* copyCS;
        ComputeShader* particleUpdate;
        ComputeShader* refraction;
        Image* refractionImage;
        struct RefractionConstStruct {
            DirectX::SimpleMath::Vector2 uvOffset = DirectX::SimpleMath::Vector2(0.f, 0.f);
            DirectX::SimpleMath::Vector2 textureDim = DirectX::SimpleMath::Vector2(0.f, 0.f);
            DirectX::SimpleMath::Vector2 targetDim = DirectX::SimpleMath::Vector2(0.f, 0.f);
            float amplitudFactor = 0.04f;
            float dudvScale = 1.f;
            float nearZ = -1.f;
            float farZ = -1.f;
            float fov = -1.f;
            float pad0 = -1;
        } refractionConstData;
    private:
        void mUpdateRefractionBuffers();
        std::map<std::string, Geometry::SuperGeometry*> mGeometry;
        std::map<std::wstring, Texture::SuperTexture*> mTexture;
        std::vector<Image*> mImages;
        std::map<const std::type_info*, std::vector<Material::SuperMaterial*>> mMaterial;
    };

    template <typename T> T* ResourceManager::CreateMaterial(std::string name) {
        const std::type_info* materialType = &typeid(T*);
        std::vector<Material::SuperMaterial*>& materialVector = mMaterial[materialType];
        for (auto& material : materialVector)
            if (material->GetName() == name)
                return static_cast<T*>(material);
        T* material = new T(name);
        materialVector.push_back(material);
        return static_cast<T*>(material);
    }

    template <typename T> unsigned int ResourceManager::GetMaterialSize() {
        const std::type_info* materialType = &typeid(T*);
        return static_cast<unsigned int>(mMaterial[materialType].size());
    }

    template <typename T> T* ResourceManager::GetMaterialAt(const unsigned int i) {
        std::size_t index = static_cast<std::size_t>(i);
        const std::type_info* materialType = &typeid(T*);
        if (index < mMaterial[materialType].size()) {
            return static_cast<T*>(mMaterial[materialType][index]);
        }
        return nullptr;
    }
}
