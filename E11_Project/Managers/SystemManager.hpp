#pragma once

namespace E11 {
    namespace System {
        class RenderSystem;
        class PhysicsSystem;
        class TransformSystem;
        class ParticleSystem;
        class UpdateSystem;
        class PickingSystem;
    }
    class SystemManager {
    public:
        SystemManager();
        ~SystemManager();
        System::RenderSystem* renderSystem;
        System::PhysicsSystem* physicsSystem;
        System::TransformSystem* transformSystem;
        System::ParticleSystem* particleSystem;
        System::UpdateSystem* updateSystem;
        System::PickingSystem* pickingSystem;
    };
}
