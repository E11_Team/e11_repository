#include "InputManager.hpp"

#include "../Core.hpp"
#include "../Window.hpp"

#include "../Entity/Entity.hpp"
#include "../Component/Transform.hpp"
#include "../Component/Lens.hpp"

using namespace E11;

using namespace DirectX;
using namespace SimpleMath;

InputManager::InputManager() {
}

InputManager::~InputManager() {
}

Vector2 InputManager::GetScreenSpaceMousePosition() {
    // get current mouse position in screen coords
    POINT pos = { 0, 0 };
    if (GetCursorPos(&pos))
    {
        // convert position to client window coords
        if (ScreenToClient(Core().window->windowHandle, &pos))
        {
            // get window's client rect
            RECT rcClient = { 0 };
            GetClientRect(Core().window->windowHandle, &rcClient);

            // if mouse cursor is inside rect
            if (PtInRect(&rcClient, pos)) {
                return Vector2(static_cast<float>(pos.x) / Core().window->GetWidth() * 2.f - 1.f, -(static_cast<float>(pos.y) / Core().window->GetHeight() * 2.f - 1.f));
            }
        }
    }
    return Vector2(0.f, 0.f);
}

Vector3 InputManager::GetMouseRay(Component::Lens* lens) {
    Vector2 mouseScreenPosition = GetScreenSpaceMousePosition();
    // Calculate mouseRay
    // http://antongerdelan.net/opengl/raycasting.html
    Vector3 ray_nds = Vector3(mouseScreenPosition.x, mouseScreenPosition.y, 1.f); // [-1:1, -1:1, -1:1]
    Vector4 ray_clip = Vector4(ray_nds.x, ray_nds.y, 1.0, 1.0);
    Matrix projectionMatrix = XMMatrixPerspectiveFovLH(lens->fov, static_cast<float>(Core().window->GetWidth()) / static_cast<float>(Core().window->GetHeight()), lens->nearZ, lens->farZ);
    Vector4 ray_eye = XMVector4Transform(ray_clip, XMMatrixInverse(nullptr, projectionMatrix));
    ray_eye = Vector4(ray_eye.x, ray_eye.y, 1.0, 0.0);

    Component::Transform* lensTransform = lens->entity->GetComponent<Component::Transform>();
    Matrix viewMatrix = XMMatrixLookToLH(lensTransform->position, lensTransform->frontDirection, lensTransform->upDirection);
    Vector4 ray_wor = XMVector4Transform(ray_eye, XMMatrixInverse(nullptr, viewMatrix));
    return Vector3(ray_wor.x, ray_wor.y, ray_wor.z) / sqrtf(ray_wor.x * ray_wor.x + ray_wor.y * ray_wor.y + ray_wor.z * ray_wor.z);
}
