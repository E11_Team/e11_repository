#pragma once

#include <SimpleMath.h>

namespace E11 {
    namespace Component {
        class Lens;
    }
    class InputManager {
    public:
        InputManager();
        ~InputManager();
        DirectX::SimpleMath::Vector2 GetScreenSpaceMousePosition();
        DirectX::SimpleMath::Vector3 GetMouseRay(Component::Lens* lens);
    };
}
