#pragma once

#include <map>
#include <string>

#include <SimpleMath.h>

struct ID3D11Buffer;
struct ID3D11ShaderResourceView;
struct ID3D11RenderTargetView;

namespace E11 {
    class ComputeShader;
    class Image;
    namespace Component {
        class Lens;
    }
    namespace ShaderProgram {
        class Copy;
    }
    class FXManager {
    public:
        FXManager();
        ~FXManager();

        ShaderProgram::Copy* copy;

        void CopyPS(Image* srcImage, Image* trgImage);
        void CopyPS(ID3D11ShaderResourceView* srcSRV, ID3D11RenderTargetView* trgRTV);
        void CopyCS(Image* srcImage, Image* trgImage);
        void CopyCPU(Image* srcImage, Image* trgImage);
        void Refraction(Image* srcImage, Image* trgImage, DirectX::SimpleMath::Vector2 uvOffset, Component::Lens* lens);

        struct ComputeShaderPack {
            ComputeShader* computeShader = nullptr;
            ID3D11Buffer* constBuffers = nullptr;
            unsigned int nrOfConstBuffers = 0;
            ID3D11ShaderResourceView* SRVs = nullptr;
            unsigned int nrOfSRVs = 0;
        };

        std::map<std::wstring, ComputeShaderPack> computeShaders;
    };
}
