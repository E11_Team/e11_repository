#include "ResourceManager.hpp"

#include <d3d11.h>

#include "../Image/Image.hpp"
#include "../Shaders/ComputeShader.hpp"

#include "../Core.hpp"
#include "../Window.hpp"

#include "../Geometry/Plane.hpp"
#include "../Geometry/OBJMesh.hpp"

#include "../Texture/Texture2D.hpp"

#include "../Material/StandardMaterial.hpp"
#include "../Material/SceneryMaterial.hpp"

using namespace E11;
using namespace Geometry;
using namespace Texture;
using namespace Material;

ResourceManager::ResourceManager() {
    copyCS = new ComputeShader(L"Resources/Shaders/Copy_CS.hlsl");
    particleUpdate = new ComputeShader(L"Resources/Shaders/Particles_CS.hlsl");

    refraction = new ComputeShader(L"Resources/Shaders/Refraction_CS.hlsl");
    refraction->textures[refraction->nrOfTextures++] = static_cast<Texture2D*>(Create2DTexture(L"Resources/Assets/ParticleRefraction.png"));

    refractionImage = new Image(Core().window->GetWidth(), Core().window->GetHeight());

    D3D11_BUFFER_DESC cbDesc;
    ZeroMemory(&cbDesc, sizeof(D3D11_BUFFER_DESC));
    cbDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    cbDesc.Usage = D3D11_USAGE_DYNAMIC;
    cbDesc.ByteWidth = sizeof(RefractionConstStruct);
    cbDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    cbDesc.MiscFlags = 0;
    cbDesc.StructureByteStride = 0;
    Core().device->CreateBuffer(&cbDesc, NULL, &refraction->constBuffers[refraction->nrOfConstBuffers++]);
    refraction->updateFunction = std::bind(&ResourceManager::mUpdateRefractionBuffers, this);
}

ResourceManager::~ResourceManager() {
    for (auto& it : mGeometry) {
        delete it.second;
    }
    mGeometry.clear();

    for (auto& it : mTexture) {
        delete it.second;
    }
    mTexture.clear();

    for (auto& image : mImages) {
        delete image;
    }
    mImages.clear();

    for (auto& it : mMaterial) {
        for (auto& material : it.second) {
            delete material;
        }
        it.second.clear();
    }
    mMaterial.clear();

    delete copyCS;
    delete particleUpdate;
    delete refraction;
    delete refractionImage;
}

Plane* ResourceManager::CreatePlane() {
    Plane* plane;
    if (mGeometry.find("plane") == mGeometry.end()) {
        plane = new Plane();
        mGeometry["plane"] = plane;
    }
    plane = static_cast<Plane*>(mGeometry["plane"]);
    return plane;
}

OBJMesh* ResourceManager::CreateOBJMesh(std::string filePath) {
    OBJMesh* objMesh;
    if (mGeometry.find(filePath) == mGeometry.end()) {
        objMesh = new OBJMesh(filePath);
        mGeometry[filePath] = objMesh;
    }
    objMesh = static_cast<OBJMesh*>(mGeometry[filePath]);
    return objMesh;
}

Texture2D* ResourceManager::Create2DTexture(std::wstring filePath) {
    Texture2D* texture;
    if (mTexture.find(filePath.c_str()) == mTexture.end()) {
        texture = new Texture2D(filePath);
        mTexture[filePath] = texture;
    }
    texture = static_cast<Texture2D*>(mTexture[filePath]);
    return texture;
}

Image* ResourceManager::CreateImage(unsigned int width, unsigned int height) {
    Image* image = new Image(width, height);
    mImages.push_back(image);
    return image;
}

void ResourceManager::mUpdateRefractionBuffers() {
    D3D11_MAPPED_SUBRESOURCE mappedResource;
    ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
    Core().deviceContext->Map(refraction->constBuffers[0], NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedResource);
    memcpy(mappedResource.pData, &refractionConstData, sizeof(RefractionConstStruct));
    Core().deviceContext->Unmap(refraction->constBuffers[0], 0);
}
