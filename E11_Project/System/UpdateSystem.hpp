#pragma once

namespace E11 {
    class Scene;
    namespace System {
        class UpdateSystem {
        public:
            UpdateSystem();
            ~UpdateSystem();
            void Update(Scene& scene);
        };
    }
}
