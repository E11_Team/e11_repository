#pragma once

#include <SimpleMath.h>

namespace E11 {
    class Scene;
    namespace Component {
        class Lens;
    }
    namespace System {
        class PickingSystem {
        public:
            PickingSystem();
            ~PickingSystem();
            void Update(Scene& scene, Component::Lens* lens);
        };
    }
}
