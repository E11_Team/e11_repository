#include "PhysicsSystem.hpp"

#include <SimpleMath.h>

#include "../Scene/Scene.hpp"
#include "../Entity/Entity.hpp"
#include "../Component/Physics.hpp"
#include "../Component/Transform.hpp"

using namespace E11;
using namespace System;

using namespace DirectX;
using namespace SimpleMath;

PhysicsSystem::PhysicsSystem() {
}

PhysicsSystem::~PhysicsSystem() {
}

void PhysicsSystem::Update(Scene& scene, const float deltaTime) {
    std::vector<Component::Physics*> physicsVec = scene.Get<Component::Physics>();
    for (auto& physics : physicsVec) {
        Component::Transform* transform = physics->entity->GetComponent<Component::Transform>();

        // Add angular velocity // TODO CHECK ORDER OF YAW PITCH ROLL
        transform->Roll(physics->angularVelocity.z * 360.f * deltaTime);
        transform->Pitch(physics->angularVelocity.y * 360.f * deltaTime);
        transform->Yaw(physics->angularVelocity.x * 360.f * deltaTime);

        // Add angular drag
        physics->angularVelocity -= physics->angularVelocity * physics->angularDragFactor * deltaTime;

        // Add velocity
        transform->position += physics->velocity * deltaTime;

        // Add drag
        physics->velocity -= physics->velocity * physics->drag * deltaTime;
    }
}