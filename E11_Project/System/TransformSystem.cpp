#include "TransformSystem.hpp"

#include <SimpleMath.h>

#include "../Scene/Scene.hpp"
#include "../Entity/Entity.hpp"
#include "../Component/Transform.hpp"
#include "../Component/Lens.hpp"

using namespace E11;
using namespace System;

using namespace DirectX;
using namespace SimpleMath;

TransformSystem::TransformSystem() {
}

TransformSystem::~TransformSystem() {
}

void TransformSystem::Update(Scene& scene) {
    // Update matrices for each transform component
    std::vector<Component::Transform*> transformVec = scene.Get<Component::Transform>();
    for (auto& transform : transformVec) {
        transform->UpdateMatrices();
    }
    // Update view matrix and view-frustum-planes for each lens component
    std::vector<Component::Lens*> lensVec = scene.Get<Component::Lens>();
    for (auto& lens : lensVec) {
        Component::Transform* lensTransform = lens->entity->GetComponent<Component::Transform>();
        lens->viewMatrix = XMMatrixLookToLH(lensTransform->position, lensTransform->frontDirection, lensTransform->upDirection);
        lens->UpdateViewFrustumPlanes();
    }
}