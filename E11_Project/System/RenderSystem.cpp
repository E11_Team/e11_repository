#include "RenderSystem.hpp"

#include <d3d11.h>
#include <SimpleMath.h>

#include "../Core.hpp"
#include "../Managers/ResourceManager.hpp"

#include "../ShaderProgram/Standard.hpp"
#include "../ShaderProgram/Particles.hpp"
//#include "../ShaderProgram/Sky.hpp" // TODO SKY SHADERPROGRAM

#include "../Scene/Scene.hpp"
#include "../Entity/Entity.hpp"
#include "../Image/Image.hpp"
#include "../Geometry/SuperGeometry.hpp"
#include "../Component/Model.hpp"
#include "../Component/Lens.hpp"
#include "../Component/Transform.hpp"
#include "../Component/PointLight.hpp"

using namespace E11;
using namespace System;
using namespace ShaderProgram;

using namespace DirectX;
using namespace SimpleMath;

RenderSystem::RenderSystem() {
    standardPipeline = new Standard(L"Resources/Shaders/Standard_VS.hlsl", L"Resources/Shaders/Standard_HS.hlsl", L"Resources/Shaders/Standard_DS.hlsl", L"Resources/Shaders/Standard_GS.hlsl", L"Resources/Shaders/Standard_PS.hlsl");
    particlesPipeline = new Particles(L"Resources/Shaders/Particles_VS.hlsl", L"Resources/Shaders/Particles_GS.hlsl", L"Resources/Shaders/Particles_PS.hlsl");
}

RenderSystem::~RenderSystem() {
    standardPipeline->UnBind();
    delete standardPipeline;

    particlesPipeline->UnBind();
    delete particlesPipeline;
}

void RenderSystem::Update(Scene& scene) {
    std::vector<Component::Lens*> lensVec = scene.Get<Component::Lens>();

    Core().resourceManager->refractionImage->ClearAll(0.f, 0.f, 0.f);
    for (auto& lens : lensVec) {
        lens->image->ClearAll(0.05f, 0.05f, 0.05f);
        mViewFrustumCulling(scene, lens);
    }

    // TODO z-prepass?

    standardPipeline->Bind();
    for (auto& lens : lensVec) {
        standardPipeline->Dispatch(scene, lens);
    }
    standardPipeline->UnBind();

    particlesPipeline->Bind();
    for (auto& lens : lensVec) {
        particlesPipeline->Dispatch(scene, lens);
    }
    particlesPipeline->UnBind();
}

void RenderSystem::mViewFrustumCulling(Scene& scene, Component::Lens* lens) {
    std::vector<Component::Model*> modelVec = scene.Get<Component::Model>();
    for (auto& model : modelVec) {
        model->cull = false;
        if (model->viewFrustumCull) {
            Component::Transform* modelTransform = model->entity->GetComponent<Component::Transform>();
            float scale = modelTransform->GetMaxScale();
            float worldRadius = model->geometry->radius * scale;
            model->cull = lens->ViewFrustumCull(modelTransform->position, worldRadius);
        }
    }

    std::vector<Component::PointLight*> pointLightVec = scene.Get<Component::PointLight>();
    for (auto& pointLight : pointLightVec) {
        pointLight->cull = false;
        if (pointLight->viewFrustumCull) {
            Component::Transform* pointLightTransform = pointLight->entity->GetComponent<Component::Transform>();
            float scale = pointLightTransform->GetMaxScale();
            float worldRadius = pointLight->radius * scale;

            pointLight->cull = lens->ViewFrustumCull(pointLightTransform->position, worldRadius);
        }
    }
}
