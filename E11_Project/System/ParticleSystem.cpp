#include "ParticleSystem.hpp"

#include <SimpleMath.h>

#include "../Core.hpp"
#include "../Managers/ResourceManager.hpp"
#include "../Shaders/ComputeShader.hpp"
#include "../Scene/Scene.hpp"
#include "../Entity/Entity.hpp"
#include "../Particle/Particle.hpp"
#include "../Component/ParticleEmitter.hpp"
#include "../Component/ParticleGravityCenter.hpp"
#include "../Component/Transform.hpp"

using namespace E11;
using namespace System;

using namespace DirectX;
using namespace SimpleMath;

ParticleSystem::ParticleSystem() {
    // Constant Buffer
    D3D11_BUFFER_DESC cbDesc;
    ZeroMemory(&cbDesc, sizeof(D3D11_BUFFER_DESC));
    cbDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    cbDesc.Usage = D3D11_USAGE_DYNAMIC;
    cbDesc.ByteWidth = sizeof(csConstStruct);
    cbDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    cbDesc.MiscFlags = 0;
    cbDesc.StructureByteStride = 0;
    Core().device->CreateBuffer(&cbDesc, NULL, &csConstBuffer);
}

ParticleSystem::~ParticleSystem() {
    csConstBuffer->Release();
}

bool myfunction(Particle& i, Particle& j) { return (i.position.z>j.position.z); }

void ParticleSystem::Update(Scene& scene, const float deltaTime) {
    DynamicArray<Particle>* particleArr = scene.GetParticleArr();
    unsigned int particleAmount = particleArr->Size();

    if (particleAmount > 0) {
        ID3D11Buffer* particleSourceBuffer = scene.GetParticleSourceBuffer();
        ID3D11Buffer* particleTargetBuffer = scene.GetParticleTargetBuffer();
        ID3D11ShaderResourceView* particleSourceSRV = scene.GetParticleSourceSRV();
        ID3D11UnorderedAccessView* particleTargetUAV = scene.GetParticleTargetUAV();
        ID3D11Resource* resource;
        D3D11_MAPPED_SUBRESOURCE mappedResource;

        std::vector<Component::ParticleGravityCenter*> gravityCenterVec = scene.Get<Component::ParticleGravityCenter>();
        unsigned int gravityCenterAmount = static_cast<unsigned int>(gravityCenterVec.size());

        // Create ParticleGravityCenter Buffer
        struct rawGravityCenter {
            Vector3 position;
            float gravityCoefficient;
        };
        ID3D11Buffer* gravityCenterBuffer;
        D3D11_BUFFER_DESC buffDesc;
        ZeroMemory(&buffDesc, sizeof(D3D11_BUFFER_DESC));
        buffDesc.ByteWidth = sizeof(rawGravityCenter) * gravityCenterAmount;
        buffDesc.Usage = D3D11_USAGE_DYNAMIC;
        buffDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
        buffDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        buffDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
        buffDesc.StructureByteStride = sizeof(rawGravityCenter);
        D3D11_SUBRESOURCE_DATA subData;
        rawGravityCenter* pData = new rawGravityCenter[gravityCenterAmount];
        for (unsigned int i = 0; i < gravityCenterAmount; ++i) {
            Component::ParticleGravityCenter* gravityCenter = gravityCenterVec[i];
            pData[i].position = gravityCenter->entity->GetComponent<Component::Transform>()->position;
            pData[i].gravityCoefficient = gravityCenter->gravityCoefficient;
        }
        subData.pSysMem = pData;
        Core().device->CreateBuffer(&buffDesc, &subData, &gravityCenterBuffer);

        // Create ParticleGravityCenter SRV
        ID3D11ShaderResourceView* gravityCenterSRV;
        D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
        ZeroMemory(&srvDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
        srvDesc.Format = DXGI_FORMAT_UNKNOWN;
        srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
        srvDesc.Buffer.ElementOffset = 0;
        srvDesc.Buffer.NumElements = gravityCenterAmount;
        Core().device->CreateShaderResourceView(gravityCenterBuffer, &srvDesc, &gravityCenterSRV);

        // Update Source Buffer
        ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
        Core().deviceContext->Map(particleSourceBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedResource);
        memcpy(mappedResource.pData, particleArr->GetArrPointer(), sizeof(Particle) * particleAmount);
        Core().deviceContext->Unmap(particleSourceBuffer, 0);

        // Update Const Buffer
        csConstData.deltaTime = deltaTime;
        csConstData.nrOfGravityCenters = gravityCenterAmount;
        //std::uniform_real_distribution<> zeroToOne(0, 1); 
        //csConstData.randomVector = Vector2(0.5f, 0.5f) * 0.01f;//;Vector2(static_cast<float>(zeroToOne(Core().randomNumberGenerator))  * 2.f - 1.f, static_cast<float>(zeroToOne(Core().randomNumberGenerator))  * 2.f - 1.f);
        ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
        Core().deviceContext->Map(csConstBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedResource);
        memcpy(mappedResource.pData, &csConstData, sizeof(csConstStruct));
        Core().deviceContext->Unmap(csConstBuffer, 0);

        // Bind resources
        Core().deviceContext->CSSetConstantBuffers(0, 1, &csConstBuffer);
        Core().deviceContext->CSSetShaderResources(1, 1, &gravityCenterSRV);
        // Dispatch
        Core().resourceManager->particleUpdate->Dispatch(particleAmount, 1, particleSourceSRV, particleTargetUAV);
        // Unbind resources
        ID3D11Buffer *const pBuff[1] = { NULL };
        Core().deviceContext->CSSetConstantBuffers(0, 1, pBuff);
        ID3D11ShaderResourceView *const pSRV[1] = { NULL };
        Core().deviceContext->CSSetShaderResources(1, 1, pSRV);
        
        delete[] pData;
        gravityCenterBuffer->Release();
        gravityCenterSRV->Release();

        // Read Target Buffer 
        // http://stackoverflow.com/questions/12790820/reading-a-memory-mapped-block-of-data-into-a-structure
        ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
        particleTargetUAV->GetResource(&resource);
        Core().deviceContext->Map(resource, 0, D3D11_MAP_READ, 0, &mappedResource);
        memcpy(particleArr->GetArrPointer(), static_cast<Particle*>(mappedResource.pData), sizeof(Particle) * particleAmount);
        Core().deviceContext->Unmap(resource, 0);
        resource->Release();

        // Remove old particles
        for (unsigned int i = 0; i < particleArr->Size(); ++i) {
            Particle* particle = particleArr->At(i);
            if (particle->elapsedTime > particle->lifeLenght) {
                particleArr->SetAt(particleArr->Pop(), i);
            }
        }
    }

    // Add new particles to scene
    std::vector<Component::ParticleEmitter*> emitterVec = scene.Get<Component::ParticleEmitter>();
    for (auto& emitter : emitterVec) {
        emitter->time += deltaTime;
        if (emitter->time > emitter->emittDelay) {
            emitter->time = 0.f;
            emitter->CreateParticle(scene);
        }
    }
}
