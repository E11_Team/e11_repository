#include "UpdateSystem.hpp"

#include "../Scene/Scene.hpp"
#include "../Entity/Entity.hpp"
#include "../Component/Update.hpp"

using namespace E11;
using namespace System;

UpdateSystem::UpdateSystem() {
}

UpdateSystem::~UpdateSystem() {
}

void UpdateSystem::Update(Scene& scene) {
    std::vector<Component::Update*> updateVec = scene.Get<Component::Update>();
    for (auto& update : updateVec) {
        update->updateFunction();
    }
}