#include "PickingSystem.hpp"

#include "../Core.hpp"
#include "../Window.hpp"

#include "../Scene/Scene.hpp"
#include "../Entity/Entity.hpp"
#include "../Component/CollisionSphere.hpp"
#include "../Component/Transform.hpp"
#include "../Component/Lens.hpp"
#include "../Managers/InputManager.hpp"

using namespace E11;
using namespace System;

using namespace DirectX;
using namespace SimpleMath;

PickingSystem::PickingSystem() {
}

PickingSystem::~PickingSystem() {
}

void PickingSystem::Update(Scene& scene, Component::Lens* lens) {
    Vector3 ray_world = Core().inputManager->GetMouseRay(lens);
    Vector3 ray_pos = lens->entity->GetComponent<Component::Transform>()->position;

    Component::CollisionSphere* closestPickedCollision = nullptr;
    std::vector<Component::CollisionSphere*> collisionVec = scene.Get<Component::CollisionSphere>();
    for (auto& collision : collisionVec) {
        collision->hit = false;
        Component::Transform* transform = collision->entity->GetComponent<Component::Transform>();
        
        float t = -1.f;
        Vector3 oc = ray_pos - transform->position;
        float b = ray_world.Dot(oc);
        float p = b*b - (oc.Dot(oc) - collision->radius * collision->radius);
        if (p >= 0) {
            p = sqrt(p);
            b = -b;

            float t1 = b + p;
            float t2 = b - p;

            if (t1 < t2 && (t1 > 0.0f && (t1 < t || t == -1.f))) {
                t = t1;
                closestPickedCollision = collision;
            }
            //t2 < t1
            else if (t2 > 0.0f && (t2 < t || t == -1.f)) {
                t = t2;
                closestPickedCollision = collision;
            }
        }
    }
    if (closestPickedCollision != nullptr)
        closestPickedCollision->hit = true;
}