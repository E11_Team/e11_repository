#pragma once

#include <SimpleMath.h>

struct ID3D11Buffer;
struct ID3D11ShaderResourceView;

namespace E11 {
    class Scene;
    struct Particle;
    namespace System {
        class ParticleSystem {
        public:
            ParticleSystem();
            ~ParticleSystem();
            void Update(Scene& scene, const float deltaTime);
            ID3D11Buffer* csConstBuffer;
            struct csConstStruct {
                float deltaTime;
                unsigned int nrOfGravityCenters;
                float pad[2];
            } csConstData;
        };
    }
}
