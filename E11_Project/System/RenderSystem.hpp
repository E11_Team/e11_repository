#pragma once

namespace E11 {
    class Scene;
    namespace ShaderProgram {
        class Standard;
        class Particles;
    }
    namespace Component {
        class Lens;
    }
    namespace System {
        class RenderSystem {
        public:
            RenderSystem();
            ~RenderSystem();
            void Update(Scene& scene);
            ShaderProgram::Standard* standardPipeline;
            ShaderProgram::Particles* particlesPipeline;
        private:
            void mViewFrustumCulling(Scene& scene, Component::Lens* lens);
        };
    }
}
