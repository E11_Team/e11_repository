#pragma once

namespace E11 {
    class Scene;
    namespace System {
        class TransformSystem {
        public:
            TransformSystem();
            ~TransformSystem();
            void Update(Scene& scene);
        };
    }
}
