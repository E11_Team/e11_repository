#pragma once

namespace E11 {
    class Scene;
    namespace System {
        class PhysicsSystem {
        public:
            PhysicsSystem();
            ~PhysicsSystem();
            void Update(Scene& scene, const float deltaTime);
        };
    }
}
