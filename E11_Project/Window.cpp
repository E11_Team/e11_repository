#include "Window.hpp"

#include <d3d11.h>

#include "Core.hpp"
#include "Managers/FXManager.hpp"
#include "Image/Image.hpp"

using namespace E11;

Window::Window(unsigned int width, unsigned int height, bool fullscreen, bool borderless, std::string title) {
    mWidth = width;
    mHeight = height;
    //mNearZ = 0.1f;
    //mFarZ = 200.f;

    HINSTANCE applicationHandle = GetModuleHandle(NULL);
    WNDCLASS windowClass;
    windowClass.style = CS_HREDRAW | CS_VREDRAW;
    windowClass.lpfnWndProc = WindowProcedure;
    windowClass.cbClsExtra = 0;
    windowClass.cbWndExtra = 0;
    windowClass.hInstance = applicationHandle;
    windowClass.hIcon = LoadIcon(0, IDI_APPLICATION);
    windowClass.hCursor = LoadCursor(0, IDC_ARROW);
    windowClass.hbrBackground = static_cast<HBRUSH>(GetStockObject(WHITE_BRUSH));
    windowClass.lpszMenuName = NULL;
    windowClass.lpszClassName = (LPCSTR)L"WindowClass";

    RegisterClass(&windowClass);

    RECT rect = { 0, 0, static_cast<long>(width), static_cast<long>(height) };
    AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, FALSE);

    windowHandle = CreateWindow(
        (LPCSTR)L"WindowClass",
        (LPCSTR)L"Window Title",
        WS_OVERLAPPEDWINDOW,
        100,
        100,
        rect.right - rect.left,
        rect.bottom - rect.top,
        NULL,
        NULL,
        applicationHandle,
        NULL
        );

    ShowWindow(windowHandle, SW_SHOWDEFAULT);
    UpdateWindow(windowHandle);

    DXGI_SWAP_CHAIN_DESC scDesc;
    scDesc.BufferDesc.Width = width;
    scDesc.BufferDesc.Height = height;
    scDesc.BufferDesc.RefreshRate.Numerator = 0;
    scDesc.BufferDesc.RefreshRate.Denominator = 0;
    scDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    scDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    scDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
    scDesc.SampleDesc.Count = 1;
    scDesc.SampleDesc.Quality = 0;
    scDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    scDesc.BufferCount = 1;
    scDesc.OutputWindow = windowHandle;
    scDesc.Windowed = true;
    scDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
    scDesc.Flags = 0;

    D3D11CreateDeviceAndSwapChain(
        nullptr,
        D3D_DRIVER_TYPE_HARDWARE,
        NULL,
        D3D11_CREATE_DEVICE_DEBUG,
        nullptr,
        0,
        D3D11_SDK_VERSION,
        &scDesc,
        &Core().swapChain,
        &Core().device,
        nullptr,
        &Core().deviceContext
        );

    // Get the back buffer from the swap chain, create a render target view of it to use as
    // the target for rendering.
    ID3D11Texture2D* backBuffer;
    Core().swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBuffer));
    Core().device->CreateRenderTargetView(backBuffer, NULL, &rtv);
    backBuffer->Release();

    D3D11_VIEWPORT vp;
    vp.TopLeftX = 0.0f;		// The top left corner's x coordinate in pixels from the window's top left corner.
    vp.TopLeftY = 0.0f;		// The top left corner's y coordinate in pixels from the window's top left corner.
    vp.Width = static_cast<float>(width);	// This viewport will cover the entire window.
    vp.Height = static_cast<float>(height);	// This viewport will cover the entire window.
    vp.MinDepth = 0.0f;		// Minimum depth value used by Direct3D is 0.0f so this is used.
    vp.MaxDepth = 1.0f;		// Maximum depth value used by Direct3D is 1.0f so this is used.

    Core().deviceContext->RSSetViewports(1, &vp);				// Set the viewport to use.
}

Window::~Window() {
    rtv->Release();
}

unsigned int Window::GetWidth() {
    return mWidth;
}

unsigned int Window::GetHeight() {
    return mHeight;
}

//float Window::GetNearZ() {
//    return mNearZ;
//}
//
//float Window::GetFarZ() {
//    return mFarZ;
//}

void Window::SetTitle(std::string title) {
    SetWindowText(windowHandle, (LPCSTR)title.c_str());
}

bool Window::Running() const {
    MSG windowMsg = { 0 };

    while (windowMsg.message != WM_QUIT)
    {
        if (PeekMessage(&windowMsg, NULL, NULL, NULL, PM_REMOVE))
        {
            TranslateMessage(&windowMsg);

            DispatchMessage(&windowMsg);
        }
        else
        {
            // If there are no more messages to handle, run a frame 
            return true;
        }
    }
    return false;
}

void Window::Close() {
}

LRESULT CALLBACK WindowProcedure(HWND handle, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;
    }

    // If a message has not been handled, send it to the default window procedure for handling.
    return DefWindowProc(handle, message, wParam, lParam);
}

void Window::Present(Image* srcImage) {
    Core().fxManager->CopyPS(srcImage->srv, rtv);
    Core().swapChain->Present(0, 0);
}
