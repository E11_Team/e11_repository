#pragma once

#include "SuperComponent.hpp"

#include <SimpleMath.h>

namespace E11 {
    class Entity;
    class Scene;
    namespace Component {
        class ParticleEmitter : public SuperComponent {
        public:
            ParticleEmitter(Entity* entity);
            ~ParticleEmitter();

            // Default 1.f (seconds)
            float emittDelay;

            // Default 0.f (seconds)
            float time;

            /// Defines a kind of particles to emitt.
            struct ParticleType {
                // The initial velocity of a particle
                // Default 0.f, 1.f, 0.f
                DirectX::SimpleMath::Vector3 velocity;

                // The initial lifetime of a particle
                // Default 1.f
                float lifeTime;

                ///// Index on texture atlas to apply to particles.
                //int textureIndex;

                ///// The minimum initial velocity in all directions.
                //glm::vec3 minVelocity;

                ///// The maximum initial velocity in all directions.
                //glm::vec3 maxVelocity;

                ///// The minimum lifetime of the particle (in seconds).
                //float minLifetime;

                ///// The maximum lifetime of the particle (in seconds).
                //float maxLifetime;

                /// The minimum size of the particle.
                DirectX::SimpleMath::Vector2 minSize;

                /// The maximum size of the particle.
                DirectX::SimpleMath::Vector2 maxSize;

                ///// Whether to scale all axes individually or uniformly.
                //bool uniformScaling;

                ///// Alpha at the beginning of the particle's life.
                //float startAlpha;

                ///// Alpha in the middle of the particle's life.
                //float midAlpha = 1.f;

                ///// Alpha at the end of the particle's life.
                //float endAlpha;

                ///// Blend color.
                //glm::vec3 color = glm::vec3(1.f, 1.f, 1.f);
            } particleType;

            void CreateParticle(Scene& scene);
        };
    }
}
