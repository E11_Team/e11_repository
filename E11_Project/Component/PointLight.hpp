#pragma once

#include "SuperComponent.hpp"

#include <SimpleMath.h>

namespace E11 {
    class Entity;
    namespace Component {
        // https://imdoingitwrong.wordpress.com/2011/01/31/light-attenuation/
        class PointLight : public SuperComponent {
        public:
            PointLight(Entity* entity);
            ~PointLight();
            // Attenuation of the light
            // Default 0.2f
            float attenuation;
            // Intensity of the light
            // Default 1.f
            float intensity;
            // Radius of light
            // Default 1.f
            float radius;
            // Color of light
            // Default (1.f, 1.f, 1.f) 
            DirectX::SimpleMath::Vector3 diffuse;
            // Whether light is to be rendered of not 
            // Default false
            bool cull;
            // Whether light should be checked for view-frustum-culling
            // Default true
            bool viewFrustumCull;
        private:
        };
    }
}
