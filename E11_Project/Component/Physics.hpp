#pragma once

#include "SuperComponent.hpp"

#include <SimpleMath.h>

namespace E11 {
    class Entity;
    namespace Component {
        class Physics : public SuperComponent {
        public:
            Physics(Entity* entity);
            ~Physics();
            // Angular velocity Yaw, Pitch, Roll (in Hz)
            // Default 0.f, 0.f, 0.f
            DirectX::SimpleMath::Vector3 angularVelocity;
            // Amount of drag
            // Default 0.f
            float angularDragFactor;
            // Velocity
            // Default 0.f, 0.f, 0.f
            DirectX::SimpleMath::Vector3 velocity;
            // Amount of drag
            // Default 0.f
            float drag;
        };
    }
}
