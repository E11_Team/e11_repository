#include "ParticleGravityCenter.hpp"

#include "../Scene/Scene.hpp"
#include "../Entity/Entity.hpp"
#include "../Particle/Particle.hpp"

#include "../Core.hpp"

using namespace DirectX;
using namespace SimpleMath;

using namespace E11;
using namespace Component;

ParticleGravityCenter::ParticleGravityCenter(Entity* entity) : SuperComponent(entity){
    gravityCoefficient = 9.82f;
}

ParticleGravityCenter::~ParticleGravityCenter() {
}
