#pragma once

#include "SuperComponent.hpp"

#include <SimpleMath.h>

namespace E11 {
    class Entity;
    class Image;
    namespace Component {
        class Lens : public SuperComponent {
        public:
            Lens(Entity* entity);
            ~Lens();
            Image* image;
            // Top-down field-of-view angle in radians
            // Default (90 degrees) 
            float fov;
            // Near clip plane distance
            // Default 0.1f
            float nearZ;
            // Far clip plane distance
            // Default 200.f
            float farZ;
            // View matrix
            DirectX::XMMATRIX viewMatrix;
            // View Frustum Planes x,y,z,d
            // Default plane-order: near, far, left, right, up, down
            struct frustumPlane {
                DirectX::SimpleMath::Vector3 n;
                float d;
            };
            struct viewFrustumField {
                frustumPlane frustumPlanes[6];
            };
            // View frustum planes of outer view frustum
            viewFrustumField viewFrustum;
            // Light grid frustum fields
            viewFrustumField* lightGridFrustumFields;
            // Light grid dimensions
            // Default 4,4
            unsigned int lightGridDimensions[2];

            // Calculate and update view frustum planes
            void UpdateViewFrustumPlanes();
            // Calculate and update light grid frustum planes (view space)
            // Only need to be done if light grid dimensions, fov or windows dimensions changes. 
            void UpdateLightGridFrustumPlanes();
            // Check if a point with radius intersects view-frustum
            bool ViewFrustumCull(const DirectX::SimpleMath::Vector3& worldPosition, const float& worldRadius);
            // Check if a point with radius intersects light grid frustum (x,y)
            bool LightGridFrustumCull(unsigned int x, unsigned int y, const DirectX::SimpleMath::Vector3& worldPosition, const float& worldRadius);
        };
    }
}
