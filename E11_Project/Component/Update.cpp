#include "Update.hpp"

using namespace E11;
using namespace Component;

Update::Update(Entity* entity) : SuperComponent(entity){
    updateFunction = std::bind(&Update::mDummyFunction, this);
}

Update::~Update() {
}

void Update::mDummyFunction() {
}
