#pragma once

#include "SuperComponent.hpp"

#include <SimpleMath.h>

namespace E11 {
    class Entity;
    class Scene;
    namespace Component {
        class ParticleGravityCenter : public SuperComponent {
        public:
            ParticleGravityCenter(Entity* entity);
            ~ParticleGravityCenter();

            // Default 9.82f
            float gravityCoefficient;
        };
    }
}
