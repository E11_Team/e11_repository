#include "Lens.hpp"

#include "../Entity/Entity.hpp"
#include "Transform.hpp"
#include "../Core.hpp"
#include "../Window.hpp"

using namespace E11;
using namespace Component;

using namespace DirectX;
using namespace SimpleMath;

Lens::Lens(Entity* entity) : SuperComponent(entity){
    image = nullptr;
    fov = XMConvertToRadians(90.f);
    nearZ = 0.1f;
    farZ = 200.f;
    viewMatrix = XMMatrixLookToLH(Vector3(0.f, 0.f, 0.f), Vector3(0.f, 0.f, 1.f), Vector3(0.f, 1.f, 0.f));
    lightGridDimensions[0] = 16;
    lightGridDimensions[1] = 8;
    lightGridFrustumFields = new viewFrustumField[lightGridDimensions[0] * lightGridDimensions[1]];
    UpdateLightGridFrustumPlanes();
}

Lens::~Lens() {
    delete lightGridFrustumFields;
}

void Lens::UpdateViewFrustumPlanes() {
    Component::Transform* transform = this->entity->GetComponent<Component::Transform>();
    // 0---1 
    // |   | 4-5
    // 2---3 6-7
    Vector3 points[8];

    // Calculate coners of the view frustum in world space
    float heightByWidth = static_cast<float>(Core().window->GetHeight()) / Core().window->GetWidth();
    float aspectRatio = 1.f/heightByWidth;
    Vector3 D = farZ * transform->frontDirection;
    Vector3 R = tan(fov/2) * farZ * transform->sideDirection * aspectRatio;
    Vector3 U = heightByWidth * R.Length() * transform->upDirection;
    float distanceRatio = nearZ / farZ;
    Vector3 d = distanceRatio * D;
    Vector3 r = distanceRatio * R;
    Vector3 u = distanceRatio * U;

    Vector3 lensPosition = transform->position;
    points[0] = lensPosition + D + U - R;
    points[1] = lensPosition + D + U + R;
    points[2] = lensPosition + D - U - R;
    points[3] = lensPosition + D - U + R;
    points[4] = lensPosition + d + u - r;
    points[5] = lensPosition + d + u + r;
    points[6] = lensPosition + d - u - r;
    points[7] = lensPosition + d - u + r;

    // Calculate frustum Planes
    // near
    viewFrustum.frustumPlanes[0].n = -transform->frontDirection;
    viewFrustum.frustumPlanes[0].d = -viewFrustum.frustumPlanes[0].n.Dot(lensPosition + d);
    // far
    viewFrustum.frustumPlanes[1].n = transform->frontDirection;
    viewFrustum.frustumPlanes[1].d = -viewFrustum.frustumPlanes[1].n.Dot(lensPosition + D);
    // left
    Vector3 point = points[0];
    Vector3 normal = (points[4] - point).Cross(points[2] - point);
    normal.Normalize();
    viewFrustum.frustumPlanes[2].n = normal;
    viewFrustum.frustumPlanes[2].d = -viewFrustum.frustumPlanes[2].n.Dot(point);
    // right
    point = points[1];
    normal = (points[3] - point).Cross(points[5] - point);
    normal.Normalize();
    viewFrustum.frustumPlanes[3].n = normal;
    viewFrustum.frustumPlanes[3].d = -viewFrustum.frustumPlanes[3].n.Dot(point);
    // up
    point = points[0];
    normal = (points[1] - point).Cross(points[4] - point);
    normal.Normalize();
    viewFrustum.frustumPlanes[4].n = normal;
    viewFrustum.frustumPlanes[4].d = -viewFrustum.frustumPlanes[4].n.Dot(point);
    // down
    point = points[2];
    normal = (points[6] - point).Cross(points[3] - point);
    normal.Normalize();
    viewFrustum.frustumPlanes[5].n = normal;
    viewFrustum.frustumPlanes[5].d = -viewFrustum.frustumPlanes[5].n.Dot(point);
}

bool Lens::ViewFrustumCull(const Vector3& worldPosition, const float& worldRadius) {
    Component::Transform* lensTransform = this->entity->GetComponent<Component::Transform>();

    Vector3 lensToModelVector = worldPosition - lensTransform->position;
    // left, right, up, down
    for (unsigned int i = 2; i < 6; ++i)
        if (this->viewFrustum.frustumPlanes[i].n.Dot(lensToModelVector - this->viewFrustum.frustumPlanes[i].n * worldRadius) > 0)
            return true;

    // Model is infront of lens, check near and far
    // near, far
    Vector3 nearPoint = lensTransform->position + lensTransform->frontDirection * this->nearZ;
    Vector3 farPoint = lensTransform->position + lensTransform->frontDirection * this->farZ;
    float a = this->viewFrustum.frustumPlanes[0].n.Dot(worldPosition - nearPoint - this->viewFrustum.frustumPlanes[0].n * worldRadius);
    float b = this->viewFrustum.frustumPlanes[1].n.Dot(worldPosition - farPoint - this->viewFrustum.frustumPlanes[1].n * worldRadius);
    // If sign of a and b are different model is outside view frustum 
    if (a * b < 0)
        return true;
    
    return false;
}

void Lens::UpdateLightGridFrustumPlanes() {

    float heightByWidth = static_cast<float>(Core().window->GetHeight()) / Core().window->GetWidth();
    float aspectRatio = 1.f / heightByWidth;
    float distanceRatio = nearZ / farZ;

    // Calculate coners of the view frustum in view space
    Vector3 D = farZ * Vector3(0.f, 0.f, 1.f);
    Vector3 R = tan(fov / 2) * farZ * Vector3(1.f, 0.f, 0.f) * aspectRatio;
    Vector3 U = heightByWidth * R.Length() * Vector3(0.f, 1.f, 0.f);
    Vector3 d = distanceRatio * D;
    Vector3 r = distanceRatio * R;
    Vector3 u = distanceRatio * U;

    Vector3 point0 = D + U - R;

    Vector3 dR = 2.f * R / static_cast<float>(lightGridDimensions[0]);
    Vector3 dU = 2.f * U / static_cast<float>(lightGridDimensions[1]);

    for (unsigned int y = 0; y < lightGridDimensions[1]; ++y) {
        for (unsigned int x = 0; x < lightGridDimensions[0]; ++x) {

            unsigned int index = lightGridDimensions[0] * y + x;

            // 0---1 
            // |   | 4-5
            // 2---3 6-7
            Vector3 points[8];
            points[0] = point0 + dR*x - dU*y;
            points[1] = point0 + dR*(x+1) - dU*y;
            points[2] = point0 + dR*x - dU*(y+1);
            points[3] = point0 + dR*(x+1) - dU*(y+1);
            points[4] = points[0] * distanceRatio;
            points[5] = points[1] * distanceRatio;
            points[6] = points[2] * distanceRatio;
            points[7] = points[3] * distanceRatio;

            //for (int i = 0; i < 8; i++)
            //    points[i] = XMVector3Transform(points[i], this->viewMatrix);

            // Calculate frustum Planes
            // near
            lightGridFrustumFields[index].frustumPlanes[0].n = Vector3(0.f, 0.f, -1.f);
            lightGridFrustumFields[index].frustumPlanes[0].d = this->nearZ;
            // far
            lightGridFrustumFields[index].frustumPlanes[1].n = Vector3(0.f, 0.f, 1.f);
            lightGridFrustumFields[index].frustumPlanes[1].d = -this->farZ;
            // left
            Vector3 point = points[0];
            Vector3 normal = (points[4] - point).Cross(points[2] - point);
            normal.Normalize();
            lightGridFrustumFields[index].frustumPlanes[2].n = normal;
            lightGridFrustumFields[index].frustumPlanes[2].d = -lightGridFrustumFields[index].frustumPlanes[2].n.Dot(point);
            // right
            point = points[1];
            normal = (points[3] - point).Cross(points[5] - point);
            normal.Normalize();
            lightGridFrustumFields[index].frustumPlanes[3].n = normal;
            lightGridFrustumFields[index].frustumPlanes[3].d = -lightGridFrustumFields[index].frustumPlanes[3].n.Dot(point);
            // up
            point = points[0];
            normal = (points[1] - point).Cross(points[4] - point);
            normal.Normalize();
            lightGridFrustumFields[index].frustumPlanes[4].n = normal;
            lightGridFrustumFields[index].frustumPlanes[4].d = -lightGridFrustumFields[index].frustumPlanes[4].n.Dot(point);
            // down
            point = points[2];
            normal = (points[6] - point).Cross(points[3] - point);
            normal.Normalize();
            lightGridFrustumFields[index].frustumPlanes[5].n = normal;
            lightGridFrustumFields[index].frustumPlanes[5].d = -lightGridFrustumFields[index].frustumPlanes[5].n.Dot(point);

        }
    }
}

bool Lens::LightGridFrustumCull(unsigned int x, unsigned int y, const DirectX::SimpleMath::Vector3& worldPosition, const float& worldRadius) {
    Vector3 viewPosition = XMVector3Transform(worldPosition, this->viewMatrix);
    unsigned int index = y * this->lightGridDimensions[0] + x;

    // left, right, up, down
    for (unsigned int i = 2; i < 6; ++i) {
        Vector3 normal = this->lightGridFrustumFields[index].frustumPlanes[i].n;
        if (normal.Dot(viewPosition - normal * worldRadius) > 0) {
            return true;
        }
    }

    // Model is infront of lens, check near and far
    // near, far
    float a = this->lightGridFrustumFields[index].frustumPlanes[0].n.Dot(viewPosition - Vector3(0.f, 0.f, this->nearZ) - this->lightGridFrustumFields[index].frustumPlanes[0].n * worldRadius);
    float b = this->lightGridFrustumFields[index].frustumPlanes[1].n.Dot(viewPosition - Vector3(0.f, 0.f, this->farZ) - this->lightGridFrustumFields[index].frustumPlanes[1].n * worldRadius);
    // If sign of a and b are different model is outside view frustum 
    if (a * b < 0)
        return true;

    return false;
}
