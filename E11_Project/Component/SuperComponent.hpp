#pragma once

namespace E11 {
    class Entity;
    namespace Component {
        class SuperComponent {
        public:
            SuperComponent(Entity* entity);
            virtual ~SuperComponent();
            Entity* entity;
        };
    }
}
