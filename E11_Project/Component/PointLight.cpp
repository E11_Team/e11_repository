#include "PointLight.hpp"

#include "../Entity/Entity.hpp"

using namespace E11;
using namespace Component;

using namespace DirectX;
using namespace SimpleMath;

PointLight::PointLight(Entity* entity) : SuperComponent(entity){
    diffuse = Vector3(1.f, 1.f, 1.f);
    attenuation = 0.2f;
    intensity = 1.f;
    radius = 1.f;
    cull = false;
    viewFrustumCull = true;
}

PointLight::~PointLight() {
}
