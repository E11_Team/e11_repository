#include "CollisionSphere.hpp"

using namespace DirectX;
using namespace SimpleMath;

using namespace E11;
using namespace Component;

CollisionSphere::CollisionSphere(Entity* entity) : SuperComponent(entity){
    radius = 1.f;
    hit = false;
}

CollisionSphere::~CollisionSphere() {
}
