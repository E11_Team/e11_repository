#include "Transform.hpp"


using namespace DirectX;
using namespace SimpleMath;

using namespace E11;
using namespace Component;

Transform::Transform(Entity* entity) : SuperComponent(entity){
    position = Vector3(0.f, 0.f, 0.f);
    rotation = XMQuaternionRotationRollPitchYaw(0.f, 0.f, 0.f);
    scale = Vector3(1.f, 1.f, 1.f);
    worldMatrix = XMMatrixIdentity();
    frontDirection = Vector3(0.f, 0.f, 1.f);
    upDirection = Vector3(0.f, 1.f, 0.f);
    sideDirection = Vector3(1.f, 0.f, 0.f);
}

Transform::~Transform() {
}

Matrix Transform::CalculateWorldMatrix() {
    return CalculateScaleMatrix() * CalculateOrientationMatrix() * CalculateTranslationMatrix();
}

Matrix Transform::CalculateTranslationMatrix() {
    return XMMatrixTranslationFromVector(position);
}

Matrix Transform::CalculateOrientationMatrix() {
    return XMMatrixRotationQuaternion(rotation);

    //http://www.cprogramming.com/tutorial/3d/quaternions.html
    //float fAngle = 0.001f;// XM_PI;
    //Vector3 axis(0, 1, 0);
    //total = XMQuaternionRotationRollPitchYaw(XM_PI,0,0); //Quaternion(1, 0, 0, 0);

    /*Quaternion local_rotation;
    local_rotation.w = cosf(fAngle / 2);
    local_rotation.x = axis.x * sinf(fAngle / 2);
    local_rotation.y = axis.y * sinf(fAngle / 2);
    local_rotation.z = axis.z * sinf(fAngle / 2);

    total = local_rotation * total;*/

    //float x = total.x;
    //float y = total.y;
    //float z = total.z;
    //float w = total.w;

    //sideDirection = Vector3(1 - 2 * y*y - 2 * z*z, 2 * x*y - 2 * w*z, 2 * x*z + 2 * w*y);
    //upDirection = Vector3(2 * x*y + 2 * w*z, 1 - 2 * x*x - 2 * z*z, 2 * y*z + 2 * w*x);
    //frontDirection = Vector3(2 * x*z - 2 * w*y, 2 * y*z - 2 * w*x, 1 - 2 * x*x - 2 * y*y);

    /*Matrix orientation(
        sideDirection.x, sideDirection.y, sideDirection.z, 0,
        upDirection.x, upDirection.y, upDirection.z, 0,
        frontDirection.x, frontDirection.y, frontDirection.z, 0,
        0, 0, 0, 1
        );*/

    //Matrix orientation(
    //    1-2*y*y-2*z*z, 2*x*y-2*w*z, 2*x*z+2*w*y, 0,
    //    2*x*y+2*w*z, 1-2*x*x-2*z*z, 2*y*z+2*w*x, 0,
    //    2*x*z-2*w*y, 2*y*z-2*w*x, 1-2*x*x-2*y*y, 0,
    //    0, 0, 0, 1
    //    );
    ////return orientation;
    //return XMMatrixRotationRollPitchYawFromVector(rotation);
}

Matrix Transform::CalculateScaleMatrix() {
    return XMMatrixScalingFromVector(scale);
}

void Transform::UpdateMatrices() {
    worldMatrix = CalculateWorldMatrix();
}

float Transform::GetMaxScale() {
    return (std::fmax)((std::fmax)(scale.x, scale.y), scale.z);
}

void Transform::Yaw(float angle) {
    RotateAxis(upDirection, angle);
}

void Transform::Pitch(float angle) {
    RotateAxis(sideDirection, angle);
}

void Transform::Roll(float angle) {
    RotateAxis(frontDirection, angle);
}

void Transform::RotateAxis(Vector3 axis, float angle) {
    angle = XMConvertToRadians(angle);
    if (fabs(1.f - (axis.x * axis.x + axis.y * axis.y + axis.z * axis.z)) > 0.01f)
        axis.Normalize();
    Quaternion local_rotation;
    local_rotation.w = cosf(angle / 2);
    local_rotation.x = axis.x * sinf(angle / 2);
    local_rotation.y = axis.y * sinf(angle / 2);
    local_rotation.z = axis.z * sinf(angle / 2);

    rotation = local_rotation * rotation;

    if (fabs(1.f - (rotation.x * rotation.x + rotation.y * rotation.y + rotation.z * rotation.z + rotation.w * rotation.w)) > 0.01f)
        rotation.Normalize();

    float x = rotation.x;
    float y = rotation.y;
    float z = rotation.z;
    float w = rotation.w;

    sideDirection = Vector3(1 - 2 * y*y - 2 * z*z, 2 * x*y - 2 * w*z, 2 * x*z + 2 * w*y);
    upDirection = Vector3(2 * x*y + 2 * w*z, 1 - 2 * x*x - 2 * z*z, 2 * y*z + 2 * w*x);
    frontDirection = Vector3(2 * x*z - 2 * w*y, 2 * y*z - 2 * w*x, 1 - 2 * x*x - 2 * y*y);

    if (fabs(1.f - (sideDirection.x * sideDirection.x + sideDirection.y * sideDirection.y + sideDirection.z * sideDirection.z)) > 0.01f)
        sideDirection.Normalize();
    if (fabs(1.f - (upDirection.x * upDirection.x + upDirection.y * upDirection.y + upDirection.z * upDirection.z)) > 0.01f)
        upDirection.Normalize();
    if (fabs(1.f - (frontDirection.x * frontDirection.x + frontDirection.y * frontDirection.y + frontDirection.z * frontDirection.z)) > 0.01f)
        frontDirection.Normalize();
}

void Transform::Move(DirectX::SimpleMath::Vector3 direction) {
    position += direction;
}

void Transform::MoveFront(float length) {
    Move(frontDirection * length);
}

void Transform::MoveUp(float length) {
    Move(upDirection * length);
}

void Transform::MoveSide(float length) {
    Move(sideDirection * length);
}
