#pragma once

#include "SuperComponent.hpp"

#include <functional>

namespace E11 {
    class Entity;
    namespace Component {
        class Update : public SuperComponent {
        public:
            Update(Entity* entity);
            ~Update();
            std::function<void()> updateFunction;
        private:
            void mDummyFunction();
        };
    }
}
