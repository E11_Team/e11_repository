#pragma once

#include "SuperComponent.hpp"

#include <SimpleMath.h>

namespace E11 {
    class Entity;
    namespace Component {
        class CollisionSphere : public SuperComponent {
        public:
            CollisionSphere(Entity* entity);
            ~CollisionSphere();
            // Radius
            // Default 1.f
            float radius;
            // Whether sphere hit by picking
            // Default false
            bool hit;
        };
    }
}
