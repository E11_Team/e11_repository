#include "Model.hpp"

#include "../Core.hpp"
#include "../Managers/ResourceManager.hpp"
#include "../Geometry/OBJMesh.hpp"
#include "../Material/StandardMaterial.hpp"

using namespace E11;
using namespace Component;

Model::Model(Entity* entity) : SuperComponent(entity){
    geometry = Core().resourceManager->CreateOBJMesh("Resources/Assets/DefaultGeometry.obj");
    diffuse = Core().resourceManager->Create2DTexture(L"Resources/Assets/DefaultDiffuse.png");
    bump = Core().resourceManager->Create2DTexture(L"Resources/Assets/DefaultBump.png");
    dispacement = Core().resourceManager->Create2DTexture(L"Resources/Assets/DefaultDisplacement.png");
    SetMaterial(Core().resourceManager->CreateMaterial<Material::StandardMaterial>("DefaultMaterial"));
    LOD = 1.f;
    displacementAmplitude = 1.f;
    cull = false;
    viewFrustumCull = true;
}

Model::~Model() {
}

void Model::SetMaterial(Material::SuperMaterial* material) {
    if (mMaterial != nullptr)
        mMaterial->RemoveModel(this);
    mMaterial = material;
    material->AddModel(this);
}
