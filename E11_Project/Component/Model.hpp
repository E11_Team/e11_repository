#pragma once

#include "SuperComponent.hpp"

#include "../Material/StandardMaterial.hpp"

namespace E11 {
    class Entity;
    namespace Texture {
        class Texture2D;
    }
    namespace Geometry {
        class SuperGeometry;
    }
    namespace Material {
        class SuperMaterial;
    }
    namespace Component {
        class Model : public SuperComponent {
        public:
            Model(Entity* entity);
            ~Model();
            Geometry::SuperGeometry* geometry;
            Texture::Texture2D* diffuse;
            Texture::Texture2D* bump;
            Texture::Texture2D* dispacement;
            void SetMaterial(Material::SuperMaterial* material);
            // Level Of Detail (Tesselation)
            // Default 1.f
            float LOD;
            // Amplitude of displacement
            // Default 0.2f
            float displacementAmplitude;
            // Whether model is to be rendered of not 
            // Default false
            bool cull;
            // Whether model should be checked for view-frustum-culling
            // Default true
            bool viewFrustumCull;
        private:
            Material::SuperMaterial* mMaterial;
        };
    }
}
