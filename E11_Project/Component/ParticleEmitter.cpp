#include "ParticleEmitter.hpp"

#include "../Scene/Scene.hpp"
#include "../Entity/Entity.hpp"
#include "../Particle/Particle.hpp"
#include "../Component/Transform.hpp"

#include "../Core.hpp"
#include <random>

using namespace DirectX;
using namespace SimpleMath;

using namespace E11;
using namespace Component;

ParticleEmitter::ParticleEmitter(Entity* entity) : SuperComponent(entity){
    emittDelay = 1.f;
    particleType.velocity = Vector3(0.f, 1.f, 0.f);
    particleType.lifeTime = 1.f;
    particleType.minSize = Vector2(1.f, 1.f);
    particleType.maxSize = Vector2(1.f, 1.f);

    time = 0.f;
}

ParticleEmitter::~ParticleEmitter() {
}

void ParticleEmitter::CreateParticle(Scene& scene) {
    std::uniform_real_distribution<> zeroToOne(0, 1);
    //std::vector<Particle*>* particleVec = scene.GetPointer<Particle>();
    DynamicArray<Particle>* particleArr = scene.GetParticleArr();
    // Create new particle
    if (particleArr->Size() < particleArr->Capacity()) {
        Particle particle;
        particle.position = entity->GetComponent<Component::Transform>()->position;
        particle.velocity = particleType.velocity;
        particle.lifeLenght = particleType.lifeTime;
        particle.scale = particleType.minSize + static_cast<float>(zeroToOne(Core().randomNumberGenerator)) * particleType.minSize * particleType.maxSize;
        particleArr->Push(particle);
    }
}
