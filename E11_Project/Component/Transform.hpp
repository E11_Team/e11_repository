#pragma once

#include "SuperComponent.hpp"

#include <SimpleMath.h>

namespace E11 {
    class Entity;
    namespace Component {
        class Transform : public SuperComponent {
        public:
            Transform(Entity* entity);
            ~Transform();

            // Default 0.f, 0.f, 0.f
            DirectX::SimpleMath::Vector3 position;

            // Default 0.f, 0.f, 1.f
            DirectX::SimpleMath::Vector3 frontDirection;

            // Default 0.f, 1.f, 0.f
            DirectX::SimpleMath::Vector3 upDirection;

            // Default 1.f, 0.f, 0.f
            DirectX::SimpleMath::Vector3 sideDirection;

            // Quaternion reprecenting rotation
            // Default direction 0.f, 0.f, 1.f
            DirectX::SimpleMath::Quaternion rotation;

            // Default 1.f, 1.f, 1.f
            DirectX::SimpleMath::Vector3 scale;

            // Default Identity matrix
            DirectX::SimpleMath::Matrix worldMatrix;

            DirectX::SimpleMath::Matrix CalculateWorldMatrix();
            DirectX::SimpleMath::Matrix CalculateTranslationMatrix();
            DirectX::SimpleMath::Matrix CalculateOrientationMatrix();
            DirectX::SimpleMath::Matrix CalculateScaleMatrix();
            void UpdateMatrices();
            float GetMaxScale();

            void RotateAxis(DirectX::SimpleMath::Vector3 axis, float angle);
            void Yaw(float angle);
            void Pitch(float angle);
            void Roll(float angle);
            void Move(DirectX::SimpleMath::Vector3 direction);
            void MoveFront(float length);
            void MoveUp(float length);
            void MoveSide(float length);
        };
    }
}
