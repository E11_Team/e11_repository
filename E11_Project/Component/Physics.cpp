#include "Physics.hpp"

using namespace DirectX;
using namespace SimpleMath;

using namespace E11;
using namespace Component;

Physics::Physics(Entity* entity) : SuperComponent(entity){
    angularVelocity = Vector3(0.f, 0.f, 0.f);
    angularDragFactor = 0.f;
    velocity = Vector3(0.f, 0.f, 0.f);
    drag = 0.f;
}

Physics::~Physics() {
}
