#pragma once

//TODO forward declare
#include <d3d11.h>

namespace E11 {
    class Window;
    class Hub {
    public:
        Hub();
        ~Hub();
        ID3D11Device* device;
        ID3D11DeviceContext* deviceContext;
        Window* window;
    };
}
