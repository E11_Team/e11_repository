#pragma once

struct ID3D11VertexShader;
struct ID3D11HullShader;
struct ID3D11DomainShader;
struct ID3D11GeometryShader;
struct ID3D11PixelShader;
struct ID3D11InputLayout;
struct ID3D11SamplerState;
struct ID3D11BlendState;
struct ID3D11DepthStencilState;
struct ID3D11RasterizerState;
struct D3D11_INPUT_ELEMENT_DESC;

#include <string>

namespace E11 {
    class Scene;
    namespace Component {
        class Lens;
    }
    namespace ShaderProgram {
        class SuperShaderProgram {
        public:
            SuperShaderProgram();
            virtual ~SuperShaderProgram();
            ID3D11InputLayout* inputLayout;
            ID3D11VertexShader* vertexShader;
            ID3D11PixelShader* pixelShader;
            ID3D11SamplerState* samplerState;
            ID3D11BlendState* blendState;
            ID3D11DepthStencilState* depthStencilState;
            ID3D11RasterizerState* rasterizerState;

            virtual void Bind();
            virtual void UnBind();
            virtual void UpdateBuffers() = 0;
            //virtual void Dispatch(Scene& scene, Component::Lens* lens) = 0;
        protected:
            void mCreateVertexShaderAndInputLayout(std::wstring shaderPath, ID3D11VertexShader** vertexShader, ID3D11InputLayout** inputLayout = nullptr, D3D11_INPUT_ELEMENT_DESC* inputDesc = NULL, unsigned int sizeOfInputDesc = 0);
            void mCreatePixelShader(std::wstring shaderPath, ID3D11PixelShader** shader);
            void mCreateHullShader(std::wstring shaderPath, ID3D11HullShader** shader);
            void mCreateDomainShader(std::wstring shaderPath, ID3D11DomainShader** shader);
            void mCreateGeometryShader(std::wstring shaderPath, ID3D11GeometryShader** shader);
        };
    }
}
