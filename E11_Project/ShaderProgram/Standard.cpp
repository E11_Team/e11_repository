#include "Standard.hpp"

#include <d3d11.h>
#include <d3dcompiler.h>
#include <SimpleMath.h>

#include "../Core.hpp"
#include "../Window.hpp"
#include "../Managers/ResourceManager.hpp"

#include "../Scene/Scene.hpp"
#include "../Entity/Entity.hpp"

#include "../Component/Transform.hpp"
#include "../Component/Lens.hpp"
#include "../Component/Model.hpp"
#include "../Component/PointLight.hpp"
#include "../Image/Image.hpp"

#include "../Geometry/SuperGeometry.hpp"

#include "../Texture/Texture2D.hpp"

#include "../Material/StandardMaterial.hpp"
#include "../Material/SceneryMaterial.hpp"

using namespace E11;
using namespace ShaderProgram;
using namespace Material;

using namespace DirectX;
using namespace SimpleMath;

Standard::Standard(std::wstring vertexShaderPath, std::wstring hullShaderPath, std::wstring domainShaderPath, std::wstring geometryShaderPath, std::wstring pixelShaderPath) : SuperShaderProgram() {
    D3D11_INPUT_ELEMENT_DESC inputDesc[] =
    {
        // For each input: Semantic name, semantic index (if multiple with the same name), 
        // input format, input slot (usually 0), byte offset (depends on the previous format
        // size), input slot class (usually INPUT_PER_VERTEX_DATA), instance data step rate
        // (always 0 when using INPUT_PER_VERTEX_DATA).
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "UV", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    };
    mCreateVertexShaderAndInputLayout(vertexShaderPath, &vertexShader, &inputLayout, inputDesc, sizeof(inputDesc));
    mCreatePixelShader(pixelShaderPath, &pixelShader);

    mCreateHullShader(hullShaderPath, &hullShader);
    mCreateDomainShader(domainShaderPath, &domainShader);
    mCreateGeometryShader(geometryShaderPath, &geometryShader);

    D3D11_SAMPLER_DESC sampDesc;
    ZeroMemory(&sampDesc, sizeof(D3D11_SAMPLER_DESC));
    sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR; //
    sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampDesc.MipLODBias = 0.f;
    sampDesc.MaxAnisotropy = 1;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sampDesc.BorderColor[0] = 1.f;
    sampDesc.BorderColor[1] = 1.f;
    sampDesc.BorderColor[2] = 1.f;
    sampDesc.BorderColor[3] = 1.f;
    sampDesc.MinLOD = -D3D11_FLOAT32_MAX;// 0.f;
    sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
    Core().device->CreateSamplerState(&sampDesc, &samplerState);

    D3D11_BLEND_DESC blendDesc;
    ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));
    blendDesc.AlphaToCoverageEnable = false;
    blendDesc.IndependentBlendEnable = false;
    blendDesc.RenderTarget[0].BlendEnable = false;
    blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
    blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
    blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
    Core().device->CreateBlendState(&blendDesc, &blendState);

    D3D11_DEPTH_STENCIL_DESC dsDesc;
    ZeroMemory(&blendDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));
    dsDesc.DepthEnable = true;
    dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
    dsDesc.DepthFunc = D3D11_COMPARISON_LESS;
    dsDesc.StencilEnable = false;
    dsDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
    dsDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
    dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
    dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
    dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
    dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
    dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    Core().device->CreateDepthStencilState(&dsDesc, &depthStencilState);

    // Rasterizer
    D3D11_RASTERIZER_DESC rDesc;
    ZeroMemory(&rDesc, sizeof(D3D11_RASTERIZER_DESC));
    rDesc.FillMode = D3D11_FILL_SOLID;
    rDesc.CullMode = D3D11_CULL_BACK;
    rDesc.FrontCounterClockwise = false;
    rDesc.DepthBias = 0;
    rDesc.SlopeScaledDepthBias = 0.0f;
    rDesc.DepthBiasClamp = 0.0f;
    rDesc.DepthClipEnable = true;
    rDesc.ScissorEnable = false;
    rDesc.MultisampleEnable = false;
    rDesc.AntialiasedLineEnable = false;
    Core().device->CreateRasterizerState(&rDesc, &rasterizerState);

    D3D11_BUFFER_DESC cbDesc;
    ZeroMemory(&cbDesc, sizeof(D3D11_BUFFER_DESC));
    cbDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    cbDesc.Usage = D3D11_USAGE_DYNAMIC;
    cbDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    cbDesc.MiscFlags = 0;
    cbDesc.StructureByteStride = 0;

    cbDesc.ByteWidth = sizeof(hsConstStruct);
    Core().device->CreateBuffer(&cbDesc, NULL, &hsConstBuffer);

    cbDesc.ByteWidth = sizeof(gsConstStruct);
    Core().device->CreateBuffer(&cbDesc, NULL, &gsConstBuffer);

    cbDesc.ByteWidth = sizeof(psConstStruct);
    Core().device->CreateBuffer(&cbDesc, NULL, &psConstBuffer);

    //cbDesc.ByteWidth = sizeof(psLightStruct);
    //Core().device->CreateBuffer(&cbDesc, NULL, &psLightBuffer);
}

Standard::~Standard() {
    UnBind();
    hsConstBuffer->Release();
    gsConstBuffer->Release();
    psConstBuffer->Release();
    //psLightBuffer->Release();
    hullShader->Release();
    domainShader->Release();
    geometryShader->Release();
}

void Standard::Bind() {
    SuperShaderProgram::Bind();
    Core().deviceContext->HSSetConstantBuffers(0, 1, &hsConstBuffer);
    Core().deviceContext->GSSetConstantBuffers(0, 1, &gsConstBuffer);
    ID3D11Buffer* const pBuff[1] = { psConstBuffer };
    Core().deviceContext->PSSetConstantBuffers(0, 1, pBuff);
    Core().deviceContext->HSSetShader(hullShader, NULL, NULL);
    Core().deviceContext->DSSetShader(domainShader, NULL, NULL);
    Core().deviceContext->GSSetShader(geometryShader, NULL, NULL);
    Core().deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
    Core().deviceContext->GSSetSamplers(0, 1, &samplerState);
}

void Standard::UnBind() {
    SuperShaderProgram::UnBind();
    ID3D11Buffer *const pBuff[1] = { NULL };
    Core().deviceContext->HSSetConstantBuffers(0, 1, pBuff);
    Core().deviceContext->GSSetConstantBuffers(0, 1, pBuff);
    ID3D11Buffer *const psBuff[2] = { NULL, NULL };
    Core().deviceContext->PSSetConstantBuffers(0, 2, psBuff);
    Core().deviceContext->HSSetShader(NULL, NULL, NULL);
    Core().deviceContext->DSSetShader(NULL, NULL, NULL);
    Core().deviceContext->GSSetShader(NULL, NULL, NULL);
    ID3D11SamplerState* const p[1] = { NULL };
    Core().deviceContext->GSSetSamplers(0, 1, p);
}

void Standard::UpdateBuffers() {
    D3D11_MAPPED_SUBRESOURCE mappedResource;
    ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

    Core().deviceContext->Map(hsConstBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedResource);
    memcpy(mappedResource.pData, &hsConstData, sizeof(hsConstStruct));
    Core().deviceContext->Unmap(hsConstBuffer, 0);

    Core().deviceContext->Map(gsConstBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedResource);
    memcpy(mappedResource.pData, &gsConstData, sizeof(gsConstStruct));
    Core().deviceContext->Unmap(gsConstBuffer, 0);

    //Core().deviceContext->Map(psLightBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedResource);
    //memcpy(mappedResource.pData, &psLightData, sizeof(psLightStruct));
    //Core().deviceContext->Unmap(psLightBuffer, 0);
}

void Standard::Dispatch(Scene& scene, Component::Lens* lens) {
    // ------ Render Scenery material ------

    // Bind resources
    ID3D11RenderTargetView* RTVs[2] = { lens->image->rtv, lens->image->worldPosRtv };
    Core().deviceContext->OMSetRenderTargets(2, RTVs, NULL);
    // The stride and offset need to be stored in variables as we need to provide pointers to
    // them when setting the vertex buffer.
    UINT vbStride = sizeof(Geometry::Vertex);
    UINT vbOffset = 0;


    //unsigned int size = Core().resourceManager->GetMaterialSize<Material::SceneryMaterial>();
    //for (unsigned int i = 0; i < size; ++i) {
    //    std::vector<Component::Model*> modelVec = Core().resourceManager->GetMaterialAt<Material::SceneryMaterial>(i)->GetModels();
    //    for (auto& model : modelVec) {

    //        // Update constant buffers
    //        Component::Transform* lensTransform = lens->entity->GetComponent<Component::Transform>();
    //        Matrix worldMatrix = model->entity->GetComponent<Component::Transform>()->worldMatrix;
    //        gsConstData.worldMatrix = XMMatrixTranspose(worldMatrix);
    //        gsConstData.wvpMatrix = XMMatrixTranspose(worldMatrix * XMMatrixLookToLH(lensTransform->position, lensTransform->frontDirection, lensTransform->upDirection) * XMMatrixPerspectiveFovLH(lens->fov, static_cast<float>(Core().window->GetWidth()) / static_cast<float>(Core().window->GetHeight()), Core().window->GetNearZ(), Core().window->GetFarZ()));

    //        psConstData.lensPosition = lensTransform->position;
    //        psConstData.nearZ = Core().window->GetNearZ();
    //        psConstData.farZ = Core().window->GetFarZ();

    //        UpdateBuffers();
    //        // Set the input layout, vertex buffer, topology and shaders to use when drawing.
    //        Core().deviceContext->IASetVertexBuffers(0, 1, &model->geometry->vertexBuffer, &vbStride, &vbOffset);

    //        // Set material
    //        ID3D11ShaderResourceView* diffuseSrv = model->diffuse->GetSRV();
    //        Core().deviceContext->PSSetShaderResources(0, 1, &diffuseSrv);

    //        // Draw the vertices
    //        Core().deviceContext->Draw(model->geometry->nrOfVertices, 0);
    //    }
    //}

    //// Unbind resources
    ID3D11RenderTargetView *const pRTV[2] = { NULL, NULL };
    //Core().deviceContext->OMSetRenderTargets(2, pRTV, NULL);
    ID3D11Buffer *const pVbff[1] = { NULL };
    //Core().deviceContext->IASetVertexBuffers(0, 1, pVbff, &vbStride, &vbOffset);
    //ID3D11ShaderResourceView* pSRV[1] = { NULL };
    //Core().deviceContext->PSSetShaderResources(0, 1, pSRV);

    // ------ Render Standard material ------

    // Bind resources
    //ID3D11RenderTargetView * RTVs[2] = { lens->image->rtv, lens->image->worldPosRtv };
    Core().deviceContext->OMSetRenderTargets(2, RTVs, lens->image->dsv);
    // The stride and offset need to be stored in variables as we need to provide pointers to
    // them when setting the vertex buffer.
    vbStride = sizeof(Geometry::Vertex);
    vbOffset = 0;

    std::vector<Component::PointLight*> pointLightVec = scene.Get<Component::PointLight>();
    psConstData.lightGridDimensions[0] = lens->lightGridDimensions[0];
    psConstData.lightGridDimensions[1] = lens->lightGridDimensions[1];
    unsigned int totalCounter = 0;
    for (unsigned int y = 0; y < psConstData.lightGridDimensions[1]; ++y) {
        for (unsigned int x = 0; x < psConstData.lightGridDimensions[0]; ++x) {
            unsigned int counter = 0;
            unsigned offset = totalCounter;
            for (unsigned int i = 0; i < psConstData.totalNumberOfLights; ++i) {
                Component::PointLight* pointLight = pointLightVec[i];
                if (!pointLight->cull) {
                    Component::Transform* pointLightTransform = pointLight->entity->GetComponent<Component::Transform>();
                    bool cull = lens->LightGridFrustumCull(x, y, pointLightTransform->position, pointLight->radius * pointLightTransform->GetMaxScale());
                    if (!cull) {
                        psConstData.lightIndexList[totalCounter / 4][totalCounter % 4] = i;
                        counter++;
                        totalCounter++;
                    }
                }
            }
            unsigned int index = x + y * psConstData.lightGridDimensions[0];
            psConstData.lightGrid[index / 2][0 + index % 2 * 2] = offset;
            psConstData.lightGrid[index / 2][1 + index % 2 * 2] = counter;
        }
    }
    for (unsigned int i = 0; i < psConstData.totalNumberOfLights; ++i) {
        Component::PointLight* pointLight = pointLightVec[i];
        Component::Transform* pointLightTransform = pointLight->entity->GetComponent<Component::Transform>();
        psConstData.pointLight[i].position = Vector4(pointLightTransform->position);
        psConstData.pointLight[i].diffuse = Vector4(pointLight->diffuse);
        psConstData.pointLight[i].attenuation = pointLight->attenuation;
        psConstData.pointLight[i].intensity = pointLight->intensity;
        psConstData.pointLight[i].radius = pointLight->radius * pointLightTransform->GetMaxScale();
    }

    Component::Transform* lensTransform = lens->entity->GetComponent<Component::Transform>();
    psConstData.lensPosition = lensTransform->position;
    psConstData.nearZ = lens->nearZ;
    psConstData.farZ = lens->farZ;
    psConstData.totalNumberOfLights = static_cast<unsigned int>(pointLightVec.size());
    psConstData.windowDimensions[0] = Core().window->GetWidth();
    psConstData.windowDimensions[1] = Core().window->GetHeight();

    D3D11_MAPPED_SUBRESOURCE mappedResource;
    ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
    Core().deviceContext->Map(psConstBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedResource);
    memcpy(mappedResource.pData, &psConstData, sizeof(psConstStruct));
    Core().deviceContext->Unmap(psConstBuffer, 0);

    XMMATRIX lensProjectionMatrix = XMMatrixPerspectiveFovLH(lens->fov, static_cast<float>(Core().window->GetWidth()) / static_cast<float>(Core().window->GetHeight()), lens->nearZ, lens->farZ);

    unsigned int size = Core().resourceManager->GetMaterialSize<Material::StandardMaterial>();
    for (unsigned int i = 0; i < size; ++i) {
        std::vector<Component::Model*> modelVec = Core().resourceManager->GetMaterialAt<Material::StandardMaterial>(i)->GetModels();
        for (auto& model : modelVec) {
            if (!model->cull) {
                // Update constant buffers
                hsConstData.LOD = model->LOD;

                XMMATRIX worldMatrix = model->entity->GetComponent<Component::Transform>()->worldMatrix;
                gsConstData.worldMatrix = XMMatrixTranspose(worldMatrix);
                gsConstData.wvpMatrix = XMMatrixTranspose(worldMatrix
                    * lens->viewMatrix
                    * lensProjectionMatrix);

                gsConstData.displacementAmplitude = model->displacementAmplitude;
                gsConstData.displacementDimensions[0] = model->dispacement->GetWidth();
                gsConstData.displacementDimensions[1] = model->dispacement->GetHeight();

                UpdateBuffers();
                // Set the input layout, vertex buffer, topology and shaders to use when drawing.
                Core().deviceContext->IASetVertexBuffers(0, 1, &model->geometry->vertexBuffer, &vbStride, &vbOffset);

                // Set material
                ID3D11ShaderResourceView* diffuseSrv = model->diffuse->GetSRV();
                ID3D11ShaderResourceView* bumpSrv = model->bump->GetSRV();
                ID3D11ShaderResourceView* displacementSrv = model->dispacement->GetSRV();
                Core().deviceContext->PSSetShaderResources(0, 1, &diffuseSrv);
                Core().deviceContext->PSSetShaderResources(1, 1, &bumpSrv);
                Core().deviceContext->PSSetShaderResources(2, 1, &displacementSrv);
                Core().deviceContext->GSSetShaderResources(0, 1, &displacementSrv);

                // Draw the vertices
                Core().deviceContext->Draw(model->geometry->nrOfVertices, 0);
            }
        }
    }

    // Unbind resources
    //ID3D11RenderTargetView *const pRTV[2] = { NULL, NULL };
    Core().deviceContext->OMSetRenderTargets(2, pRTV, NULL);
    //ID3D11Buffer *const pVbff[1] = { NULL };
    Core().deviceContext->IASetVertexBuffers(0, 1, pVbff, &vbStride, &vbOffset);
    ID3D11ShaderResourceView* const pSRV[3] = { NULL, NULL, NULL };
    Core().deviceContext->PSSetShaderResources(0, 3, pSRV);
}
