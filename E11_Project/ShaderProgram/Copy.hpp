#pragma once

#include "SuperShaderProgram.hpp"

struct ID3D11Buffer;
struct ID3D11ShaderResourceView;
struct ID3D11RenderTargetView;

namespace E11 {
    class Scene;
    class Image;
    namespace ShaderProgram {
        class Copy : public SuperShaderProgram {
        public:
            Copy(std::wstring vertexShaderPath, std::wstring pixelShaderPath);
            ~Copy();
            void Bind();
            void UnBind();
            void UpdateBuffers();
            void Dispatch(ID3D11ShaderResourceView* srcSRV, ID3D11RenderTargetView* trgRTV);
        private:
        };
    }
}
