//#pragma once
//
//#include "SuperShaderProgram.hpp"
//
//#include <SimpleMath.h>
//
//struct ID3D11Buffer;
//
//namespace E11 {
//    class Scene;
//    namespace Component {
//        class Lens;
//    }
//    namespace ShaderProgram {
//        class Scenery : public SuperShaderProgram {
//        public:
//            Scenery(std::wstring vertexShaderPath, std::wstring pixelShaderPath);
//            ~Scenery();
//            ID3D11Buffer* gsConstBuffer;
//            struct gsConstStruct {
//                DirectX::SimpleMath::Matrix worldMatrix, viewMatrix, projMatrix;
//            } gsConstData;
//            ID3D11Buffer* psConstBuffer;
//            struct psConstStruct {
//                DirectX::SimpleMath::Vector3 lensPosition;
//                float nearZ;
//                float farZ;
//                float pad0, pad1, pad2 = -1.f;
//            } psConstData;
//            
//            ID3D11HullShader* hullShader;
//            ID3D11DomainShader* domainShader;
//            ID3D11GeometryShader* geometryShader;
//
//            void Bind();
//            void UnBind();
//            void UpdateBuffers();
//            void Dispatch(Scene& scene, Component::Lens* lens);
//        private:
//        };
//    }
//}
