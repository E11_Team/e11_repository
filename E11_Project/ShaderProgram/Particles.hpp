#pragma once

#include "SuperShaderProgram.hpp"

#include <SimpleMath.h>
#include <random>
#include "../Particle/Particle.hpp"

struct ID3D11Buffer;
struct ID3D11GeometryShader;
struct ID3D11ShaderResourceView;

namespace E11 {
    class Scene;
    struct Particle;
    namespace Component {
        class Lens;
    }
    namespace Texture {
        class Texture2D;
    }
    namespace ShaderProgram {
        class Particles : public SuperShaderProgram {
        public:
            Particles(std::wstring vertexShaderPath, std::wstring geometryShaderPath, std::wstring pixelShaderPath, unsigned int particleCap = 300000);
            ~Particles();
            ID3D11GeometryShader* geometryShader;
            Texture::Texture2D* altasTexture;
            ID3D11Buffer* gsConstBuffer;
            struct gsConstStruct {
                DirectX::SimpleMath::Matrix viewMatrix, vpMatrix;
                DirectX::SimpleMath::Vector3 lensPosition;
                float pad0;
                DirectX::SimpleMath::Vector4 viewFrustumPlanes[6];
                DirectX::SimpleMath::Vector3 nearPoint;
                float pad1;
                DirectX::SimpleMath::Vector3 farPoint;
                float pad2;
            } gsConstData;
            ID3D11Buffer* psConstBuffer;
            struct psConstStruct {
                DirectX::SimpleMath::Vector3 lensPosition;
                float nearZ;
                float farZ;
                float pad[3];
            } psConstData;
            unsigned int mParticleCap;
            rawParticle* mRawParticleArr;

            void Bind();
            void UnBind();
            void UpdateBuffers();
            void Dispatch(Scene& scene, Component::Lens* lens);
        };
    }
}
