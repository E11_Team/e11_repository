#include "Particles.hpp"

#include <d3d11.h>
#include <d3dcompiler.h>
#include <SimpleMath.h>
#include <algorithm>

#include "../Core.hpp"
#include "../Window.hpp"
#include "../Managers/ResourceManager.hpp"
#include "../Scene/Scene.hpp"
#include "../Entity/Entity.hpp"
#include "../Component/Transform.hpp"
#include "../Component/Lens.hpp"
#include "../Image/Image.hpp"
#include "../Texture/Texture2D.hpp"

using namespace E11;
using namespace ShaderProgram;

using namespace DirectX;
using namespace SimpleMath;

Particles::Particles(std::wstring vertexShaderPath, std::wstring geometryShaderPath, std::wstring pixelShaderPath, unsigned int particleCap) : SuperShaderProgram() {
    D3D11_INPUT_ELEMENT_DESC inputDesc[] =
    {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "SCALE", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    };
    mCreateVertexShaderAndInputLayout(vertexShaderPath, &vertexShader, &inputLayout, inputDesc, sizeof(inputDesc));
    mCreatePixelShader(pixelShaderPath, &pixelShader);

    mCreateGeometryShader(geometryShaderPath, &geometryShader);

    D3D11_SAMPLER_DESC sampDesc;
    ZeroMemory(&sampDesc, sizeof(D3D11_SAMPLER_DESC));
    sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampDesc.MipLODBias = 0.f;
    sampDesc.MaxAnisotropy = 1;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sampDesc.BorderColor[0] = 1.f;
    sampDesc.BorderColor[1] = 1.f;
    sampDesc.BorderColor[2] = 1.f;
    sampDesc.BorderColor[3] = 1.f;
    sampDesc.MinLOD = -D3D11_FLOAT32_MAX;
    sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
    Core().device->CreateSamplerState(&sampDesc, &samplerState);

    // -- DEFAULT BLEND --
    //D3D11_BLEND_DESC blendDesc;
    //ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));
    //blendDesc.AlphaToCoverageEnable = false;
    //blendDesc.IndependentBlendEnable = false;
    //blendDesc.RenderTarget[0].BlendEnable = false;
    //blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
    //blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
    //blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    //blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
    //blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
    //blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    //blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
    //Core().device->CreateBlendState(&blendDesc, &blendState);

    ////http://www.gamedev.net/topic/596801-d3d11-alpha-transparency-trouble/ //Alpha blending
    //D3D11_BLEND_DESC blendDesc;
    //ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));
    //blendDesc.AlphaToCoverageEnable = false;
    //blendDesc.IndependentBlendEnable = false;
    //blendDesc.RenderTarget[0].BlendEnable = TRUE;
    //blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
    //blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
    //blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    //blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
    //blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
    //blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    //blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
    //Core().device->CreateBlendState(&blendDesc, &blendState);

    //https://takinginitiative.wordpress.com/2010/04/09/directx-10-tutorial-6-transparency-and-alpha-blending/ //Additive blending
    D3D11_BLEND_DESC blendDesc;
    ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));
    blendDesc.AlphaToCoverageEnable = false;
    blendDesc.IndependentBlendEnable = true;

    blendDesc.RenderTarget[0].BlendEnable = true;
    blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
    blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

    blendDesc.RenderTarget[1].BlendEnable = true;
    blendDesc.RenderTarget[1].SrcBlend = D3D11_BLEND_SRC_ALPHA;
    blendDesc.RenderTarget[1].DestBlend = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[1].BlendOp = D3D11_BLEND_OP_MAX; //D3D11_BLEND_OP_ADD
    blendDesc.RenderTarget[1].SrcBlendAlpha = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[1].DestBlendAlpha = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[1].BlendOpAlpha = D3D11_BLEND_OP_MAX;
    blendDesc.RenderTarget[1].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

    blendDesc.RenderTarget[2].BlendEnable = FALSE;
    blendDesc.RenderTarget[2].SrcBlend = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[2].DestBlend = D3D11_BLEND_ZERO;
    blendDesc.RenderTarget[2].BlendOp = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[2].SrcBlendAlpha = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[2].DestBlendAlpha = D3D11_BLEND_ZERO;
    blendDesc.RenderTarget[2].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[2].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

    Core().device->CreateBlendState(&blendDesc, &blendState);

    D3D11_DEPTH_STENCIL_DESC dsDesc;
    ZeroMemory(&blendDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));
    dsDesc.DepthEnable = true;
    dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
    dsDesc.DepthFunc = D3D11_COMPARISON_LESS; // D3D11_COMPARISON_LESS_EQUAL
    dsDesc.StencilEnable = false;
    dsDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
    dsDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
    dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
    dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
    dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
    dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
    dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    Core().device->CreateDepthStencilState(&dsDesc, &depthStencilState);

    // Constant Buffers
    D3D11_BUFFER_DESC cbDesc;
    ZeroMemory(&cbDesc, sizeof(D3D11_BUFFER_DESC));
    cbDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    cbDesc.Usage = D3D11_USAGE_DYNAMIC;
    cbDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    cbDesc.MiscFlags = 0;
    cbDesc.StructureByteStride = 0;

    cbDesc.ByteWidth = sizeof(gsConstStruct);
    Core().device->CreateBuffer(&cbDesc, NULL, &gsConstBuffer);

    cbDesc.ByteWidth = sizeof(psConstStruct);
    Core().device->CreateBuffer(&cbDesc, NULL, &psConstBuffer);

    altasTexture = Core().resourceManager->Create2DTexture(L"Resources/Assets/Atlas2.png");

    mParticleCap = particleCap;
    mRawParticleArr = new rawParticle[mParticleCap];
}

Particles::~Particles() {
    UnBind();
    gsConstBuffer->Release();
    geometryShader->Release();
    psConstBuffer->Release();
    delete[] mRawParticleArr;
}

void Particles::Bind() {
    SuperShaderProgram::Bind();
    Core().deviceContext->GSSetConstantBuffers(0, 1, &gsConstBuffer);
    Core().deviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);
    Core().deviceContext->GSSetShader(geometryShader, NULL, NULL);
    Core().deviceContext->PSSetConstantBuffers(0, 1, &psConstBuffer);
}

void Particles::UnBind() {
    SuperShaderProgram::UnBind();
    ID3D11Buffer *const pBuff[1] = { NULL };
    Core().deviceContext->GSSetConstantBuffers(0, 1, pBuff);
    Core().deviceContext->PSSetConstantBuffers(0, 1, pBuff);
    Core().deviceContext->GSSetShader(NULL, NULL, NULL);
    Core().deviceContext->PSSetConstantBuffers(0, 1, pBuff);
}

void Particles::UpdateBuffers() {
    D3D11_MAPPED_SUBRESOURCE mappedResource;
    ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
    Core().deviceContext->Map(gsConstBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedResource);
    memcpy(mappedResource.pData, &gsConstData, sizeof(gsConstStruct));
    Core().deviceContext->Unmap(gsConstBuffer, 0);

    ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
    Core().deviceContext->Map(psConstBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedResource);
    memcpy(mappedResource.pData, &psConstData, sizeof(psConstStruct));
    Core().deviceContext->Unmap(psConstBuffer, 0);
}

void Particles::Dispatch(Scene& scene, Component::Lens* lens) {
    DynamicArray<Particle>* particleArr = scene.GetParticleArr();
    unsigned int particleAmount = particleArr->Size();
    if (particleAmount > 0) {
        // --- Render particles ---
        // Bind resources
        ID3D11ShaderResourceView* const pSRVs[1] = { altasTexture->GetSRV()};
        Core().deviceContext->PSSetShaderResources(0, 1, pSRVs);

        ID3D11RenderTargetView* const RTVs[3] = { lens->image->rtv, Core().resourceManager->refractionImage->rtv, lens->image->worldPosRtv };
        Core().deviceContext->OMSetRenderTargets(3, RTVs, lens->image->dsv);

        Component::Transform* lensTransform = lens->entity->GetComponent<Component::Transform>();
        Matrix viewMatrix = lens->viewMatrix;
        Matrix viewProjMatix = viewMatrix * XMMatrixPerspectiveFovLH(lens->fov, static_cast<float>(Core().window->GetWidth()) / static_cast<float>(Core().window->GetHeight()), lens->nearZ, lens->farZ);;

        gsConstData.viewMatrix = XMMatrixTranspose(viewMatrix);
        gsConstData.vpMatrix = XMMatrixTranspose(viewProjMatix);
        gsConstData.lensPosition = lensTransform->position;
        for (unsigned int i = 0; i < 6; ++i)
            gsConstData.viewFrustumPlanes[i] = Vector4(lens->viewFrustum.frustumPlanes[i].n.x, lens->viewFrustum.frustumPlanes[i].n.y, lens->viewFrustum.frustumPlanes[i].n.z, -1.f);
        gsConstData.nearPoint = lensTransform->position + lensTransform->frontDirection * lens->nearZ;
        gsConstData.farPoint = lensTransform->position + lensTransform->frontDirection * lens->farZ;

        psConstData.lensPosition = lensTransform->position;
        psConstData.nearZ = lens->nearZ;
        psConstData.farZ = lens->farZ;

        UpdateBuffers();

        unsigned int particleToDrawAmount = 0;
        for (unsigned int i = 0; i < particleAmount; ++i) {
            Particle* particle = particleArr->At(i);
            if (!particle->cull) {
                mRawParticleArr[particleToDrawAmount].position = particle->position;
                mRawParticleArr[particleToDrawAmount].scale = particle->scale;
                particleToDrawAmount++;
            }
        }

        ID3D11Buffer* particleBuffer = scene.GetParticleVertexBuffer();

        D3D11_MAPPED_SUBRESOURCE mappedResource;
        ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
        Core().deviceContext->Map(particleBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedResource);
        memcpy(mappedResource.pData, mRawParticleArr, sizeof(rawParticle) * particleToDrawAmount);
        Core().deviceContext->Unmap(particleBuffer, 0);

        UINT vbStride = sizeof(rawParticle);
        UINT vbOffset = 0;
        Core().deviceContext->IASetVertexBuffers(0, 1, &particleBuffer, &vbStride, &vbOffset);

        // Draw quad
        Core().deviceContext->Draw(particleToDrawAmount, 0);

        // Unbind resources
        ID3D11ShaderResourceView *const pSRV[2] = { NULL, NULL };
        Core().deviceContext->PSSetShaderResources(0, 2, pSRV);
        ID3D11RenderTargetView *const pRTV[3] = { NULL, NULL, NULL };
        Core().deviceContext->OMSetRenderTargets(3, pRTV, NULL);
        ID3D11Buffer *const pVbff[1] = { NULL };
        Core().deviceContext->IASetVertexBuffers(0, 1, pVbff, &vbStride, &vbOffset);
    }
}
