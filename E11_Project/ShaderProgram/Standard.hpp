#pragma once

#include "SuperShaderProgram.hpp"

#include <SimpleMath.h>

struct ID3D11Buffer;

namespace E11 {
    class Scene;
    namespace Component {
        class Lens;
    }
    namespace ShaderProgram {
        class Standard : public SuperShaderProgram {
        public:
            Standard(std::wstring vertexShaderPath, std::wstring hullShaderPath, std::wstring domainShaderPath, std::wstring geometryShaderPath, std::wstring pixelShaderPath);
            ~Standard();
            ID3D11Buffer* hsConstBuffer;
            struct hsConstStruct {
                float LOD = 1.f;
                float pad[3];
            } hsConstData;
            ID3D11Buffer* gsConstBuffer;
            struct gsConstStruct {
                DirectX::SimpleMath::Matrix worldMatrix, wvpMatrix;
                float displacementAmplitude = 1.f;
                unsigned int displacementDimensions[2];
                float pad;
            } gsConstData;

            ID3D11Buffer* psConstBuffer;
            struct rawPointLight {
                DirectX::SimpleMath::Vector4 position, diffuse;
                float intensity, attenuation, radius, pad;
            };
            struct psConstStruct {
                DirectX::SimpleMath::Vector3 lensPosition;
                float nearZ;
                float farZ;
                unsigned int totalNumberOfLights;
                unsigned int windowDimensions[2];
                rawPointLight pointLight[128];
                unsigned int lightGridDimensions[2] = { 16, 8 };
                float pading[2];
                unsigned int lightGrid[64][4]; // lightGridDimensions.x * lightGridDimensions.y / 2
                unsigned int lightIndexList[2048][4]; // MaxNrOfLights / 4 * lightGridDimensions.x * lightGridDimensions.y
            } psConstData;

            //ID3D11Buffer* psLightBuffer;
            //struct psLightStruct {
            //    unsigned int dimensions[2] = { 1, 1 };
            //    float pad[2];
            //    pointLightIndexList localPointLightIndexList[1][1];
            //} psLightData;
            
            ID3D11HullShader* hullShader;
            ID3D11DomainShader* domainShader;
            ID3D11GeometryShader* geometryShader;

            void Bind();
            void UnBind();
            void UpdateBuffers();
            void Dispatch(Scene& scene, Component::Lens* lens);
        private:
        };
    }
}
