#include "Copy.hpp"

#include <d3d11.h>
#include <d3dcompiler.h>
#include <SimpleMath.h>

#include "../Core.hpp"
#include "../Window.hpp"
#include "../Scene/Scene.hpp"
#include "../Entity/Entity.hpp"
#include "../Component/Model.hpp"
#include "../Component/Transform.hpp"
#include "../Component/Lens.hpp"
#include "../Image/Image.hpp"
#include "../Geometry/SuperGeometry.hpp"
#include "../Texture/Texture2D.hpp"

using namespace E11;
using namespace ShaderProgram;

using namespace DirectX;
using namespace SimpleMath;

Copy::Copy(std::wstring vertexShaderPath, std::wstring pixelShaderPath) : SuperShaderProgram() {
    mCreateVertexShaderAndInputLayout(vertexShaderPath, &vertexShader);
    mCreatePixelShader(pixelShaderPath, &pixelShader);

    D3D11_SAMPLER_DESC sampDesc;
    ZeroMemory(&sampDesc, sizeof(D3D11_SAMPLER_DESC));
    sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampDesc.MipLODBias = 0.f;
    sampDesc.MaxAnisotropy = 1;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sampDesc.BorderColor[0] = 1.f;
    sampDesc.BorderColor[1] = 1.f;
    sampDesc.BorderColor[2] = 1.f;
    sampDesc.BorderColor[3] = 1.f;
    sampDesc.MinLOD = -D3D11_FLOAT32_MAX;
    sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
    Core().device->CreateSamplerState(&sampDesc, &samplerState);

    D3D11_BLEND_DESC blendDesc;
    ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));
    blendDesc.AlphaToCoverageEnable = false;
    blendDesc.IndependentBlendEnable = false;
    blendDesc.RenderTarget[0].BlendEnable = false;
    blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
    blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
    blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
    Core().device->CreateBlendState(&blendDesc, &blendState);
}

Copy::~Copy() {
    UnBind();
}

void Copy::Bind() {
    SuperShaderProgram::Bind();
    Core().deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
}

void Copy::UnBind() {
    SuperShaderProgram::UnBind();
    ID3D11Buffer *const pBuff[1] = { NULL };
    Core().deviceContext->PSSetConstantBuffers(0, 1, pBuff);
}

void Copy::UpdateBuffers() {

}

void Copy::Dispatch(ID3D11ShaderResourceView* srcSRV, ID3D11RenderTargetView* trgRTV) {
    // Bind resources
    Core().deviceContext->OMSetRenderTargets(1, &trgRTV, NULL);

    //UpdateBuffers();

    Core().deviceContext->PSSetShaderResources(0, 1, &srcSRV);

    // Draw the vertices
    Core().deviceContext->Draw(4, 0);

    // Unbind resources
    ID3D11RenderTargetView *const pRTV[1] = { NULL };
    Core().deviceContext->OMSetRenderTargets(1, pRTV, NULL);
    ID3D11ShaderResourceView* pSRV[1] = { NULL };
    Core().deviceContext->PSSetShaderResources(0, 1, pSRV);
}
