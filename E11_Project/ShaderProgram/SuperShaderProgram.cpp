#include "SuperShaderProgram.hpp"

#include <d3d11.h>
#include <d3dcompiler.h>

#include "../Core.hpp"

using namespace E11;
using namespace ShaderProgram;

SuperShaderProgram::SuperShaderProgram() {
    vertexShader = nullptr;
    pixelShader = nullptr;
    samplerState = nullptr;
    blendState = nullptr;
    depthStencilState = nullptr;
    rasterizerState = nullptr;
}

SuperShaderProgram::~SuperShaderProgram() {
    UnBind();
    if (inputLayout != nullptr)
        inputLayout->Release();
    vertexShader->Release();
    pixelShader->Release();
    samplerState->Release();
    blendState->Release();
    if (rasterizerState != nullptr)
        rasterizerState->Release();
    if (depthStencilState != nullptr)
        depthStencilState->Release();
}

void SuperShaderProgram::Bind() {
    Core().deviceContext->VSSetShader(vertexShader, NULL, NULL);
    Core().deviceContext->PSSetShader(pixelShader, NULL, NULL);
    Core().deviceContext->PSSetSamplers(0, 1, &samplerState);
    float blendFactor[] = {0.f, 0.f, 0.f, 0.f};
    UINT sampleMask = 0xffffffff;
    Core().deviceContext->OMSetBlendState(blendState, blendFactor, sampleMask);
    Core().deviceContext->OMSetDepthStencilState(depthStencilState, NULL);
    Core().deviceContext->RSSetState(rasterizerState);
    Core().deviceContext->IASetInputLayout(inputLayout);
}

void SuperShaderProgram::UnBind() {
    Core().deviceContext->VSSetShader(vertexShader, NULL, NULL);
    Core().deviceContext->PSSetShader(pixelShader, NULL, NULL);
    ID3D11SamplerState *const pSS[1] = { NULL };
    Core().deviceContext->PSSetSamplers(0, 1, pSS);
    Core().deviceContext->OMSetBlendState(NULL, NULL, NULL);
    Core().deviceContext->OMSetDepthStencilState(NULL, NULL);
    Core().deviceContext->RSSetState(NULL);
    Core().deviceContext->IASetInputLayout(NULL);
}

void SuperShaderProgram::mCreateVertexShaderAndInputLayout(std::wstring shaderPath, ID3D11VertexShader** shader, ID3D11InputLayout** inputLayout, D3D11_INPUT_ELEMENT_DESC* inputDesc, unsigned int sizeOfInputDesc) {
    // Compile and create vertex shader from the file vertexShader.hlsl in the folder Resources/Shaders/.
    ID3DBlob* compiledShader = nullptr;	// A variable to hold the compiled vertex shader data.
    ID3DBlob* errorBlob = nullptr;
    HRESULT hr = D3DCompileFromFile(
        shaderPath.c_str() ,		// The path to the shader file relative to the .vxproj folder.
        nullptr,		// We don't use any defines.
        nullptr,		// We don't have any includes.
        "main",			// The name of the entry function. Must match function in source data.
        "vs_5_0",		// The shader model to use, "vs" specifies it is a vertex shader, 5_0 that it is shader model 5.0.
        0,				// No shader compile options.
        0,				// Ignored when compiling a shader (effect compile options).
        &compiledShader,	// [out] Compiled shader data.
        &errorBlob			// [out] Compile time error data.
        );
    if (FAILED(hr)) {
        std::string errorMsg = (char*)errorBlob->GetBufferPointer();
        OutputDebugStringA(errorMsg.c_str());
        errorBlob->Release();
    }

    Core().device->CreateVertexShader(
        compiledShader->GetBufferPointer(),
        compiledShader->GetBufferSize(),
        NULL,
        shader
        );

    if (inputDesc != nullptr) {
        // Create the input layout to go with our vertex shader (the layout is validated against
        // the shader's input signature).
        int inputLayoutSize = sizeOfInputDesc / sizeof(D3D11_INPUT_ELEMENT_DESC);
        Core().device->CreateInputLayout(
            inputDesc,
            inputLayoutSize,
            compiledShader->GetBufferPointer(),
            compiledShader->GetBufferSize(),
            inputLayout
            );
        compiledShader->Release();
    }
}

void SuperShaderProgram::mCreatePixelShader(std::wstring shaderPath, ID3D11PixelShader** shader) {
    ID3DBlob* compiledShader = nullptr;
    ID3DBlob* errorBlob = nullptr;
    HRESULT hr = D3DCompileFromFile(
        shaderPath.c_str(),
        nullptr,
        nullptr,
        "main",
        "ps_5_0",
        0,
        0,
        &compiledShader,
        &errorBlob
        );
    if (FAILED(hr)) {
        std::string errorMsg = (char*)errorBlob->GetBufferPointer();
        OutputDebugStringA(errorMsg.c_str());
        errorBlob->Release();
    }

    Core().device->CreatePixelShader(
        compiledShader->GetBufferPointer(),
        compiledShader->GetBufferSize(),
        NULL,
        shader
        );
    compiledShader->Release();
}

void SuperShaderProgram::mCreateHullShader(std::wstring shaderPath, ID3D11HullShader** shader) {
    ID3DBlob* compiledShader = nullptr;
    ID3DBlob* errorBlob = nullptr;
    HRESULT hr = D3DCompileFromFile(
        shaderPath.c_str(),
        nullptr,
        nullptr,
        "main",
        "hs_5_0",
        0,
        0,
        &compiledShader,
        &errorBlob
        );
    if (FAILED(hr)) {
        std::string errorMsg = (char*)errorBlob->GetBufferPointer();
        OutputDebugStringA(errorMsg.c_str());
        errorBlob->Release();
    }

    Core().device->CreateHullShader(
        compiledShader->GetBufferPointer(),
        compiledShader->GetBufferSize(),
        NULL,
        shader
        );
    compiledShader->Release();
}

void SuperShaderProgram::mCreateDomainShader(std::wstring shaderPath, ID3D11DomainShader** shader) {
    ID3DBlob* compiledShader = nullptr;
    ID3DBlob* errorBlob = nullptr;
    HRESULT hr = D3DCompileFromFile(
        shaderPath.c_str(),
        nullptr,
        nullptr,
        "main",
        "ds_5_0",
        0,
        0,
        &compiledShader,
        &errorBlob
        );
    if (FAILED(hr)) {
        std::string errorMsg = (char*)errorBlob->GetBufferPointer();
        OutputDebugStringA(errorMsg.c_str());
        errorBlob->Release();
    }

    Core().device->CreateDomainShader(
        compiledShader->GetBufferPointer(),
        compiledShader->GetBufferSize(),
        NULL,
        shader
        );
    compiledShader->Release();
}

void SuperShaderProgram::mCreateGeometryShader(std::wstring shaderPath, ID3D11GeometryShader** shader) {
    ID3DBlob* compiledShader = nullptr;
    ID3DBlob* errorBlob = nullptr;
    HRESULT hr = D3DCompileFromFile(
        shaderPath.c_str(),
        nullptr,
        nullptr,
        "main",
        "gs_5_0",
        0,
        0,
        &compiledShader,
        &errorBlob
        );
    if (FAILED(hr)) {
        std::string errorMsg = (char*)errorBlob->GetBufferPointer();
        OutputDebugStringA(errorMsg.c_str());
        errorBlob->Release();
    }

    Core().device->CreateGeometryShader(
        compiledShader->GetBufferPointer(),
        compiledShader->GetBufferSize(),
        NULL,
        shader
        );
    compiledShader->Release();
}
