#include "E11.hpp"

using namespace E11;

#include "Core.hpp"

Device::Device(unsigned int winWidth, unsigned int winHeight) {
    Core().Init(winWidth, winHeight);
}

Device::~Device() {
}

Window* Device::GetWindow() {
    return Core().window;
}

ResourceManager* Device::GetResourceManager() {
    return Core().resourceManager;
}

SystemManager* Device::GetSystemManager() {
    return Core().systemManager;
}

InputManager* Device::GetInputManager() {
    return Core().inputManager;
}

FXManager* Device::GetFXManager() {
    return Core().fxManager;
}