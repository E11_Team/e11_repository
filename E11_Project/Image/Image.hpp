#pragma once

struct ID3D11RenderTargetView;
struct ID3D11ShaderResourceView;
struct ID3D11UnorderedAccessView;
struct ID3D11DepthStencilView;

#include <SimpleMath.h>

namespace E11 {
    class Image {
    public:
        Image(unsigned int width, unsigned int height);
        ~Image();
        ID3D11RenderTargetView* rtv;
        ID3D11ShaderResourceView* srv;
        ID3D11UnorderedAccessView* uav;
        ID3D11DepthStencilView* dsv;
        ID3D11RenderTargetView* worldPosRtv;
        ID3D11ShaderResourceView* worldPosSrv;
        ID3D11UnorderedAccessView* worldPosUav;
        unsigned int GetWidth();
        unsigned int GetHeight();
        void ClearAll(float r = 0.f, float g = 0.f, float b = 0.f, float a = 1.f);
        void ClearDepthBuffer();
    private:
        unsigned int mWidth;
        unsigned int mHeight;
    };
}
