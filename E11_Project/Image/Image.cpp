#include "Image.hpp"

#include <d3d11.h>

#include "../Core.hpp"

using namespace E11;

Image::Image(unsigned int width, unsigned int height) {
    mWidth = width;
    mHeight = height;

    D3D11_TEXTURE2D_DESC textureDesc;
    ZeroMemory(&textureDesc, sizeof(textureDesc));
    textureDesc.Width = width;
    textureDesc.Height = height;
    textureDesc.MipLevels = 1;
    textureDesc.ArraySize = 1;
    textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    textureDesc.SampleDesc.Count = 1;
    textureDesc.SampleDesc.Quality = 0;
    textureDesc.Usage = D3D11_USAGE_DEFAULT;
    textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
    textureDesc.CPUAccessFlags = 0;
    textureDesc.MiscFlags = 0;

    D3D11_TEXTURE2D_DESC depthDsvDesc;
    ZeroMemory(&depthDsvDesc, sizeof(depthDsvDesc));
    depthDsvDesc.Width = width;
    depthDsvDesc.Height = height;
    depthDsvDesc.MipLevels = 1;
    depthDsvDesc.ArraySize = 1;
    depthDsvDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthDsvDesc.SampleDesc.Count = 1;
    depthDsvDesc.SampleDesc.Quality = 0;
    depthDsvDesc.Usage = D3D11_USAGE_DEFAULT;
    depthDsvDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    depthDsvDesc.CPUAccessFlags = 0;
    depthDsvDesc.MiscFlags = 0;

    D3D11_TEXTURE2D_DESC worldTexDesc;
    ZeroMemory(&worldTexDesc, sizeof(worldTexDesc));
    worldTexDesc.Width = width;
    worldTexDesc.Height = height;
    worldTexDesc.MipLevels = 1;
    worldTexDesc.ArraySize = 1;
    worldTexDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    worldTexDesc.SampleDesc.Count = 1;
    worldTexDesc.SampleDesc.Quality = 0;
    worldTexDesc.Usage = D3D11_USAGE_DEFAULT;
    worldTexDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
    worldTexDesc.CPUAccessFlags = 0;
    worldTexDesc.MiscFlags = 0;

    ID3D11Texture2D* texture;
    Core().device->CreateTexture2D(&textureDesc, NULL, &texture);

    Core().device->CreateRenderTargetView(texture, NULL, &rtv);
    Core().device->CreateShaderResourceView(texture, NULL, &srv);
    Core().device->CreateUnorderedAccessView(texture, NULL, &uav);
    texture->Release();

    Core().device->CreateTexture2D(&depthDsvDesc, NULL, &texture);
    Core().device->CreateDepthStencilView(texture, NULL, &dsv);
    texture->Release();

    Core().device->CreateTexture2D(&worldTexDesc, NULL, &texture);
    Core().device->CreateRenderTargetView(texture, NULL, &worldPosRtv);
    Core().device->CreateShaderResourceView(texture, NULL, &worldPosSrv);
    Core().device->CreateUnorderedAccessView(texture, NULL, &worldPosUav);
    texture->Release();



    //D3D11_TEXTURE2D_DESC depthTexDesc;
    //ZeroMemory(&depthTexDesc, sizeof(depthTexDesc));
    //depthTexDesc.Width = width;
    //depthTexDesc.Height = height;
    //depthTexDesc.MipLevels = 1;
    //depthTexDesc.ArraySize = 1;
    //depthTexDesc.Format = DXGI_FORMAT_R32_TYPELESS;
    //depthTexDesc.SampleDesc.Count = 1;
    //depthTexDesc.SampleDesc.Quality = 0;
    //depthTexDesc.Usage = D3D11_USAGE_DEFAULT;
    //depthTexDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
    //depthTexDesc.CPUAccessFlags = 0;
    //depthTexDesc.MiscFlags = 0;

    //D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
    //ZeroMemory(&srvDesc, sizeof(srvDesc));
    //srvDesc.Format = DXGI_FORMAT_R32_FLOAT;
    //srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    //srvDesc.Texture2D.MipLevels = 1;
    //srvDesc.Texture2D.MostDetailedMip = 0;

    //D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
    //ZeroMemory(&dsvDesc, sizeof(dsvDesc));
    //dsvDesc.Format = DXGI_FORMAT_D32_FLOAT;
    //dsvDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    //dsvDesc.Texture2D.MipSlice = 0;

    //Core().device->CreateTexture2D(&depthTexDesc, NULL, &texture);
    //Core().device->CreateDepthStencilView(texture, &dsvDesc, &dsv);
    //Core().device->CreateShaderResourceView(texture, &srvDesc, &depthSrv);
    //texture->Release();
}

Image::~Image() {
    rtv->Release();
    srv->Release();
    uav->Release();
    dsv->Release();
    worldPosRtv->Release();
    worldPosSrv->Release();
    worldPosUav->Release();
}

unsigned int Image::GetWidth() {
    return mWidth;
}

unsigned int Image::GetHeight() {
    return mHeight;
}

void Image::ClearAll(float r, float g, float b, float a) {
    float bgColor[] = { r, g, b, a };
    float bgWorldPos[] = { 0.f, 0.f, 0.f, 0.f };
    Core().deviceContext->ClearRenderTargetView(rtv, bgColor);
    Core().deviceContext->ClearRenderTargetView(worldPosRtv, bgWorldPos);
    ClearDepthBuffer();
}

void Image::ClearDepthBuffer() {
    Core().deviceContext->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);
}
