//cbuffer constBuffer : register(b0)
//{
//    float3 lensPosition;
//    float nearZ;
//    float farZ;
//    float3 pad;
//};

//struct PSInput
//{
//    float4 position : SV_POSITION;
//    float3 worldPos : WORLDPOS;
//    float3 normal : NORMAL;
//    float2 uv : UV;
//};

//struct PSOutput
//{
//    float4 diffuse : SV_TARGET0;
//    float4 worldPos : SV_TARGET1;
//};

//Texture2D txDiffuse : register(t0);

//SamplerState sampClamp : register(s0);

//PSOutput main(PSInput input) : SV_TARGET
//{
//    PSOutput output;
//    float3 depthVector = lensPosition - input.worldPos;
//    float depth = length(depthVector);
//    float3 diffuse = txDiffuse.Sample(sampClamp, input.uv).rgb;
//    //float linDepth = sqrt(depthVector.x * depthVector.x + depthVector.y * depthVector.y + depthVector.z * depthVector.z) / (farZ - nearZ);
//    //float expDepth = -((2.0f * nearZ) / linDepth - farZ - nearZ) / (farZ - nearZ);
//    output.diffuse = float4(diffuse, 1.f);
//    output.worldPos = float4(input.worldPos, depth);
//    return output;
//}
 