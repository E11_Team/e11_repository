struct VSInput
{
    float3 position : POSITION;
    float3 normal : NORMAL;
    float2 uv : UV;
};
 
struct VSOutput
{
    float3 position : POSITION;
    float3 normal : NORMAL;
    float2 uv : UV;
};

VSOutput main(VSInput input)
{
    VSOutput output;
 
    output.position = input.position;
    output.normal = input.normal;
    output.uv = input.uv;

    return output;
}
 