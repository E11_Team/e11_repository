struct PointLight {
    float4 position;
    float4 diffuse;
    float attenuation;
    float intensity;
    float radius;
    float pad;
};

struct LightGrid {
    uint offset;
    uint size;
    float2 pad;
};

cbuffer constBuffer : register(b0)
{
    float3 lensPosition;
    float nearZ;
    float farZ;
    uint totalNumberOfLights;
    uint2 windowDimensions;
    PointLight globalPointLightList[128];
    uint2 indexListDimensions;
    float2 pading;
    uint4 lightGrid[64]; // 16x8 / 2
    uint4 lightIndexList[2048]; // MaxNrOfLights / 4 * indexListDimensions.x * indexListDimensions.y
};

//cbuffer lightBuffer : register(b1)
//{
//    uint2 dimensions;
//    float2 pading;
//    PointLightIndexList localPointLightIndexList[1][1];
//};

struct PSInput
{
    float4 position : SV_POSITION;
    float3 worldPos : WORLDPOS;
    float3 normal : NORMAL;
    float2 uv : UV;
};

struct PSOutput
{
    float4 diffuse : SV_TARGET0;
    float4 worldPos : SV_TARGET1;
};

Texture2D txDiffuse : register(t0);
Texture2D txNormal : register(t1);
Texture2D txHeight : register(t2);

SamplerState sampClamp : register(s0);

float3 CalculateDiffuse(float2 uv) {
    return txDiffuse.Sample(sampClamp, uv).rgb;
}

// http://stackoverflow.com/questions/5255806/how-to-calculate-tangent-and-binormal
float3 CalculateNormal(float3 worldPos, float2 uv, float3 inNormal) {
    // Sample bump-map
    float3 normalValue = 2.f * txNormal.Sample(sampClamp, uv).rgb - 1.f;

    // compute derivations of the world position
    float3 p_dx = ddx(worldPos);
    float3 p_dy = ddy(worldPos);
    // compute derivations of the texture coordinate
    float2 tc_dx = ddx(uv);
    float2 tc_dy = ddy(uv);
    // compute initial tangent and bi-tangent
    float3 t = normalize(tc_dy.y * p_dx - tc_dx.y * p_dy);
    float3 b = normalize(tc_dy.x * p_dx - tc_dx.x * p_dy); // sign inversion
                                                         // get new tangent from a given mesh normal
    float3 n = normalize(inNormal);
    float3 x = cross(n, t);
    t = cross(x, n);
    t = normalize(t);
    // get updated bi-tangent
    x = cross(b, n);
    b = cross(n, x);
    b = normalize(b);
    float3x3 tbn = float3x3(t, b, n);

    return normalize(mul(normalValue, tbn));
}

float CalculateExposure(float3 lightDirection, float3 normal) {
    return max(dot(normal, lightDirection), 0.f);
}

float CalculateIllumination(float distance, float attenuation, float intensity, float radius) {
    float k = max(1.f - pow(distance, attenuation) / (pow(radius, attenuation)), 0.f);
    return intensity * k;
    //return lightIntensity / (lightAttenuation.x + lightAttenuation.y*2.f / lightRadius*distance + lightAttenuation.z / (lightRadius*lightRadius)*distance*distance);
}

float3 CalculateFinalDiffuse(float3 diffuse, float exposure, float illumination, float3 lightDiffuse) {
    return diffuse * exposure * illumination * lightDiffuse;
}

// TODO http://de.slideshare.net/DICEStudio/directx-11-rendering-in-battlefield-3 CS light cull
PSOutput main(PSInput input) : SV_TARGET
{
    PSOutput output;

    float2 clipPos = input.position.xy / windowDimensions;
    uint2 gridIndex = clipPos * indexListDimensions;
    uint listIndex = gridIndex.x + gridIndex.y * indexListDimensions.x;

    // ----- Light ----- //
    float3 finalDiffuse = float3(0.f, 0.f, 0.f);
    // Number of Lights that shades this texel
    uint offset = lightGrid[listIndex / 2][0 + listIndex % 2 * 2]; // offset
    uint size = lightGrid[listIndex / 2][1 + listIndex % 2 * 2]; // size
    // Skip shading if no lights affects this texel
    if (size > 0) {
        float3 diffuseValue = CalculateDiffuse(input.uv);
        float3 normalValue = CalculateNormal(input.worldPos, input.uv, input.normal);

        // Calculate color for each light
        for (uint i = offset; i < offset + size; ++i) {

            uint index = lightIndexList[i / 4][i % 4];
            PointLight pointLight = globalPointLightList[index];

            float3 lightWorldPos = pointLight.position.xyz;
            float3 lightDiffuse = pointLight.diffuse.xyz;
            float lightAttenuation = pointLight.attenuation;
            float lightIntensity = pointLight.intensity;
            float lightRadius = pointLight.radius;

            float3 lightVector = lightWorldPos - input.worldPos;
            float distance = length(lightVector);
            float3 lightDirection = normalize(lightVector);

            float exposure = CalculateExposure(lightDirection, normalValue);
            float illumination = CalculateIllumination(distance, lightAttenuation, lightIntensity, lightRadius);

            finalDiffuse += CalculateFinalDiffuse(diffuseValue, exposure, illumination, lightDiffuse);
        }
    }

    // TODO http://sirkan.iit.bme.hu/~szirmay/egdisfinal3.pdf iterative parallax mapping    

    float3 depthVector = lensPosition - input.worldPos;
    float depth = length(depthVector);

    //float linDepth = sqrt(depthVector.x * depthVector.x + depthVector.y * depthVector.y + depthVector.z * depthVector.z) / (farZ - nearZ);
    //float expDepth = -((2.0f * nearZ) / linDepth - farZ - nearZ) / (farZ - nearZ);
    
    output.diffuse = float4(finalDiffuse, 1.f);
    output.worldPos = float4(input.worldPos, depth);

    return output;
}
 