RWTexture2D<float4> Target : register(u0);

Texture2D<float4> Source : register(t0);

[numthreads(32, 32, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
    Target[threadID.xy] = Source[threadID.xy];
}

