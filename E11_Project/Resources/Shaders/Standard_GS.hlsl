cbuffer constBuffer
{
    float4x4 worldMatrix;
    float4x4 wvpMatrix;
    float displacementAmplitude;
    uint2 displacementDimensions;
    float pad;
};

struct GSInput
{
    float3 position : POSITION;
    float3 normal : NORMAL;
    float2 uv : UV;
};
 
struct GSOutput
{
    float4 position : SV_POSITION;
    float3 worldPos : WORLDPOS;
    float3 normal : NORMAL;
    float2 uv : UV;
};

Texture2D txDispacement : register(t0);

SamplerState sampClamp : register(s0);

// TODO http://blog.selfshadow.com/publications/blending-in-detail/ Blending several bump-maps
float3 AddNormals(float3 n1, float3 n2) {
    return normalize(float3(n1.xy + n2.xy, n1.z*n2.z));
}

// http://stackoverflow.com/questions/5281261/generating-a-normal-map-from-a-height-map
float4 CalculateBump(float2 uv, float3 inNormal) {
    float2 size = float2(2.f, 0.f) / displacementDimensions;
    const int3 offset = int3(-1, 0, 1);

    float4 heightValue = txDispacement.SampleLevel(sampClamp, uv, 0, 0) * displacementAmplitude;

    // 00 10 20
    // 01 11 21
    // 02 12 22

    float s11 = heightValue.x;
    float s01 = txDispacement.SampleLevel(sampClamp, uv, 0, offset.xy).x * displacementAmplitude;
    float s21 = txDispacement.SampleLevel(sampClamp, uv, 0, offset.zy).x * displacementAmplitude;
    float s10 = txDispacement.SampleLevel(sampClamp, uv, 0, offset.yx).x * displacementAmplitude;
    float s12 = txDispacement.SampleLevel(sampClamp, uv, 0, offset.yz).x * displacementAmplitude;
    float3 va = normalize(float3(size.xy, s21 - s01));
    float3 vb = normalize(float3(size.yx, s12 - s10));
    float3 calculatedNormal = normalize(cross(va, vb));
    float3 outNormal = AddNormals(calculatedNormal, inNormal);
    return float4(outNormal, s11);
}

float3 CalculateTangent(float3 worldPos[3], float2 uv[3]) {
    float3 edge1 = worldPos[1] - worldPos[0];
    float3 edge2 = worldPos[2] - worldPos[0];
    float2 deltaUV1 = uv[1] - uv[0];
    float2 deltaUV2 = uv[2] - uv[0];
    return normalize((deltaUV2.y * edge1 - deltaUV1.y * edge2) / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y));
}

[maxvertexcount(3)]
void main(triangle GSInput input[3], inout TriangleStream<GSOutput> TriStream)
{
    GSOutput output;

    for (uint i = 0; i < 3; ++i)
    {
        float4 bump = CalculateBump(input[i].uv, input[i].normal);
        float3 position = input[i].position + input[i].normal * bump.w;
        output.position = mul(float4(position, 1.f), wvpMatrix);
        output.worldPos = mul(float4(position, 1.f), worldMatrix).xyz;
        output.normal = normalize(mul(float4(bump.xyz, 0.f), worldMatrix)).xyz;
        output.uv = input[i].uv;
        TriStream.Append(output);
    }
}
