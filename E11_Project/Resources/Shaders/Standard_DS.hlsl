struct DSInput
{
    float3 position : POSITION;
    float3 normal : NORMAL;
    //float3 tangent : TANGENT;
    //float3 binormal : BINORMAL;
    float2 uv : UV;
};

// Output control point
struct DSOutput
{
    float3 position : POSITION;
    float3 normal : NORMAL;
    float2 uv : UV;
};

// Output patch constant data.
struct DSConstInput
{
    float edges[3] : SV_TessFactor;
    float inside : SV_InsideTessFactor;
};

[domain("tri")]
DSOutput main(
    DSConstInput constdata,
    const OutputPatch<DSInput, 3> op,
    uint pid : SV_PrimitiveID,
    float3 uvwCoord : SV_DomainLocation)
{
    DSOutput output;

    output.position = uvwCoord.x * op[0].position + uvwCoord.y * op[1].position + uvwCoord.z * op[2].position;
    output.normal = uvwCoord.x * op[0].normal + uvwCoord.y * op[1].normal + uvwCoord.z * op[2].normal;
    output.uv = uvwCoord.x * op[0].uv + uvwCoord.y * op[1].uv + uvwCoord.z * op[2].uv;

    return output;
}
