struct PSInput
{
    float4 position : SV_POSITION;
    float2 uv : UV;
};

Texture2D txSource : register(t0);

SamplerState sampClamp : register(s0);

float4 main(PSInput input) : SV_TARGET
{
    float4 output;
 
    output = txSource.Sample(sampClamp, input.uv);
 
    return output;
}
 