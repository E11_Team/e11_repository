cbuffer constBuffer : register(b0)
{
    float4x4 viewMatrix;
    float4x4 vpMatrix;
    float3 lensPosition;
    float pad0;
    float4 frustumPlaneNormals[6];
    float3 nearPoint;
    float pad1;
    float3 farPoint;
    float pad2;
};

struct GSInput
{
    float4 position : SV_POSITION;
    float2 scale : SCALE;
};

struct GSOutput
{
    float4 position : SV_POSITION;
    float2 uv : UV;
    float3 worldPos : WORLDPOS;
};

[maxvertexcount(6)]
void main(point GSInput input[1], inout TriangleStream<GSOutput> TriStream)
{
    GSOutput output;

    float3 worldPosition = input[0].position.xyz;
    float2 scale = input[0].scale;

    // ----- View-frustum-culling ----- //
    float3 lensToParticleVector = worldPosition - lensPosition;
    bool cull = false;
    float cullRadius = max(scale.x, scale.y);
    uint i;

    // left, right, up, down
    for (i = 2; i < 6 && !cull; ++i) {
        float3 normal = frustumPlaneNormals[i].xyz;
        if (dot(normal, lensToParticleVector - normal * cullRadius) > 0) {
            cull = true;
        }
    }

    // Particle is infront of lens, check near and far
    if (!cull) {
        // near, far
        float3 nearNormal = frustumPlaneNormals[0].xyz;
        float3 farNormal = frustumPlaneNormals[1].xyz;
        float a = dot(nearNormal, worldPosition - nearPoint - nearNormal * cullRadius);
        float b = dot(farNormal, worldPosition - farPoint - farNormal * cullRadius);
        // If sign of a and b are different model is outside view frustum 
        if (a * b < 0)
            cull = true;
    }

    // Particle inside view-frustum
    if (!cull) {

        // ----- Generate billboarded particle ----- //
        float3 sideDirection = normalize(float3(viewMatrix._m00, viewMatrix._m01, -viewMatrix._m02));
        float3 upDirection = normalize(float3(-viewMatrix._m10, viewMatrix._m11, viewMatrix._m12));
        float3 frontDirection = normalize(float3(-viewMatrix._m20, viewMatrix._m21, viewMatrix._m22));

        float3 particleFrontDirection = normalize(lensPosition - worldPosition);
        float3 paticleSideDirection = cross(particleFrontDirection, float3(0.f, 1.f, 0.f));
        float3 paticleUpDirection = cross(paticleSideDirection, particleFrontDirection);

        for (uint i = 0; i < 4; ++i)
        {
            float x = i == 1 || i == 3;
            float y = i == 0 || i == 1;
            output.position.xyz = worldPosition + paticleSideDirection * (x * 2.f - 1.f) * scale.x + paticleUpDirection * (y * 2.f - 1.f) * scale.y;
            output.position.w = input[0].position.w;
            output.uv.x = x;
            output.uv.y = 1.f - y;
            output.worldPos = output.position.xyz;
            output.position = mul(output.position, vpMatrix);

            TriStream.Append(output);
        }
    }
}
