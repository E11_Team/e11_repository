cbuffer constBuffer : register (b0)
{
    float2 uvOffset;
    float2 textureDim;
    float2 targetDim;
    float amplitudFactor;
    float dudvScale;
    float nearZ;
    float farZ;
    float fov;
    float pad;
};

Texture2D<float4> Source : register(t0);

Texture2D<float4> txDUDV : register(t1);

Texture2D<float4> txDUDVAmplitud : register(t2);

Texture2D<float4> txWorldPos : register(t3);

RWTexture2D<float4> Target : register(u0);

SamplerState sampWrap : register(s0);

SamplerState sampClamp : register(s1);

[numthreads(32, 32, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
    float2 screenUV = threadID.xy / targetDim;
    float4 worldPosValue = txWorldPos[threadID.xy];
    float3 worldPos = worldPosValue.xyz;
    float depth = worldPosValue.w;
    float dudvDepthScale = 2.f * sin(fov) * depth;

    float2 dudvUV = uvOffset + threadID.xy / (textureDim * dudvScale);
    float2 dudvValue = (txDUDV.SampleLevel(sampWrap, dudvUV, 0).xy * 2.f - 1.f) * 1.f / dudvDepthScale;
    float amplitudIntensity = txDUDVAmplitud[threadID.xy].x;
    float2 sampleUV = dudvValue * amplitudIntensity * amplitudFactor + screenUV;
            
    float dvduDepth = txWorldPos.SampleLevel(sampClamp, sampleUV, 0).w;

    // TODO no if statement FIXED? 
    //if (dvduDepth < depth)
    //    sampleUV = screenUV;
    float sampleFactor = clamp((depth - dvduDepth) * 1000000.f, 0.f, 1.f);
    sampleUV = screenUV * sampleFactor + sampleUV * (1.f - sampleFactor);

    // float linDepth = (2.0f * nearZ) / (farZ + nearZ - expDepth * (farZ - nearZ));
    // float expDepth = -((2.0f * nearZ) / inDepth - farZ - nearZ) / (farZ - nearZ);

    Target[threadID.xy] = Source.SampleLevel(sampClamp, sampleUV, 0);
}

