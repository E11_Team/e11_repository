struct VSInput
{
    float3 position : POSITION;
    float2 scale : SCALE;
};

struct VSOutput
{
    float4 position : SV_POSITION;
    float2 scale : SCALE;
};
 
VSOutput main(VSInput input)
{
    VSOutput output;

    output.position = float4(input.position, 1.f);
    output.scale = input.scale;

    return output;
}