cbuffer constBuffer : register (b0)
{
    float LOD;
    float3 pad;
};

// Input control point
struct HSInput
{
    float3 position : POSITION;
    float3 normal : NORMAL;
    float2 uv : UV;
};

// Output control point
struct HSOutput
{
    float3 position : POSITION;
    float3 normal : NORMAL;
    float2 uv : UV;
};

// Output patch constant data.
struct HSConstOutput
{
    float edges[3] : SV_TessFactor;
    float inside : SV_InsideTessFactor;
};

// Patch Constant Function
HSConstOutput HSConstFunction(
    InputPatch<HSInput, 3> ip,
    OutputPatch<HSOutput, 3> op,
    uint pid : SV_PrimitiveID)
{
    HSConstOutput output;

    float tessellationAmount = LOD;
    //float tessFactor = 0.5;

    // Set the tessellation factors for the three edges of the triangle.
    output.edges[0] = tessellationAmount;
    output.edges[1] = tessellationAmount;
    output.edges[2] = tessellationAmount;

    // Set the tessellation factor for tessallating inside the triangle.
    output.inside = tessellationAmount;

    return output;
}

[domain("tri")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("HSConstFunction")]
HSOutput main(
    InputPatch<HSInput, 3> ip,
    uint cpid : SV_OutputControlPointID,
    uint pid : SV_PrimitiveID)
{
    HSOutput output;

    // Insert code to compute Output here.
    output.position = ip[cpid].position;
    output.normal = ip[cpid].normal;
    output.uv = ip[cpid].uv;

    return output;
}