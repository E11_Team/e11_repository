struct Particle
{
    float3 position;
    float2 scale;
    float3 velocity;
    float maxVelocity;
    float3 acceleration;
    float lifeLenght;
    float elapsedTime;
    float drag;
    bool cull;
};

struct GravityCenter
{
    float3 position;
    float gravityCoefficient;
};

cbuffer constBuffer : register(b0)
{
    float deltaTime;
    uint nrOfGravityCenters;
    float2 pad;
};

StructuredBuffer<Particle> Source : register(t0);

StructuredBuffer<GravityCenter> GravityCenters : register(t1);

RWStructuredBuffer<Particle> Target : register(u0);

[numthreads(1, 1, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
    Target[threadID.x] = Source[threadID.x];
    Target[threadID.x].elapsedTime += deltaTime;

    // Update particle velocity and acceleration
    float3 velocity = Source[threadID.x].velocity;
    float3 acceleration = Source[threadID.x].acceleration;
    for (uint i = 0; i < nrOfGravityCenters; i++)
    {
        // Acceleration
        float3 lengthVector = GravityCenters[i].position - Source[threadID.x].position;
        float length = lengthVector.x * lengthVector.x + lengthVector.y * lengthVector.y + lengthVector.z * lengthVector.z;
        acceleration += normalize(lengthVector + 0.000001f) * length * GravityCenters[i].gravityCoefficient; // lengthVector + 0.000001f -> lengthVector != 0
        // Velocity
        velocity += acceleration * deltaTime;
        // Cap Velocity to MaxVelocity and add drag
        float velocityLength = sqrt(velocity.x * velocity.x + velocity.y * velocity.y + velocity.z * velocity.z);
        velocity = normalize(velocity) * min(velocityLength, Source[threadID.x].maxVelocity);
    }

    Target[threadID.x].position += Source[threadID.x].velocity * deltaTime;
    // Add drag to velocity
    Target[threadID.x].velocity = velocity - velocity * Source[threadID.x].drag * deltaTime;
    Target[threadID.x].acceleration = float3(0.f, 0.f, 0.f);
}
