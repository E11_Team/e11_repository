#pragma once

// Link to needed lib files. Can also be done by adding these to
// Properties -> Linker -> Input -> Additional Dependencies
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "D3DCompiler.lib")

#include "Window.hpp"
#include "Managers/ResourceManager.hpp"
#include "Managers/SystemManager.hpp"
#include "Managers/InputManager.hpp"
#include "Managers/FXManager.hpp"

#include "Scene/Scene.hpp"
#include "Image/Image.hpp"
#include "Entity/Entity.hpp"

#include "System/RenderSystem.hpp"
#include "System/PhysicsSystem.hpp"
#include "System/TransformSystem.hpp"
#include "System/ParticleSystem.hpp"
#include "System/UpdateSystem.hpp"
#include "System/PickingSystem.hpp"

#include "Component/Model.hpp"
#include "Component/Transform.hpp"
#include "Component/Lens.hpp"
#include "Component/Physics.hpp"
#include "Component/ParticleEmitter.hpp"
#include "Component/ParticleGravityCenter.hpp"
#include "Component/Update.hpp"
#include "Component/CollisionSphere.hpp"
#include "Component/PointLight.hpp"

#include "Geometry/Plane.hpp"
#include "Geometry/OBJMesh.hpp"

#include "Material/StandardMaterial.hpp"
#include "Material/SceneryMaterial.hpp"

using namespace DirectX;
using namespace SimpleMath;

namespace E11 {
    class Device {
    public:
        Device(unsigned int winWidth = 1024, unsigned int winHeight = 720);
        ~Device();
        Window* GetWindow();
        ResourceManager* GetResourceManager();
        SystemManager* GetSystemManager();
        InputManager* GetInputManager();
        FXManager* GetFXManager();
    private:
    };
}
