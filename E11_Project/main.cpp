#include "E11.hpp"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include <chrono>
#include <iostream>

using namespace E11;

class Camera {
public:
    Camera(Scene* scene, E11::Device* engine);
    ~Camera();
    Entity* node;
private:
    void mUpdate();
};

class GravityCenter {
public: 
    GravityCenter(Scene* scene, E11::Device* engine);
    ~GravityCenter();
    Entity* node;
private:
    void mUpdate();
};

int main()
{
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
    E11::Device engine(800, 800);
    E11::Window* window = engine.GetWindow();
    E11::ResourceManager* resourceManager = engine.GetResourceManager();
    E11::SystemManager* systemManager = engine.GetSystemManager();
    E11::InputManager* inputManager = engine.GetInputManager();
    E11::FXManager* fxManager = engine.GetFXManager();

    Scene scene;

    Entity* batman = scene.CreateEntity();
    batman->AddComponent<Component::Transform>()->position = Vector3(0.f, 0.f, 0.f);
    batman->AddComponent<Component::Physics>()->angularVelocity = Vector3(1.f, 0.f, 0.f) * 0.1f;
    batman->AddComponent<Component::Model>()->geometry = resourceManager->CreateOBJMesh("Resources/Assets/ninjastar.obj");
    batman->GetComponent<Component::Model>()->viewFrustumCull = false;
    //batman->GetComponent<Component::Model>()->diffuse = resourceManager->Create2DTexture(L"Resources/Assets/StoneRoadDiffuseRGBSpecularA.png");
    batman->AddComponent<Component::ParticleEmitter>()->emittDelay = 0.2f;
    batman->GetComponent<Component::ParticleEmitter>()->particleType.minSize *= 0.5f;
    batman->GetComponent<Component::ParticleEmitter>()->particleType.maxSize *= 0.75f;
    batman->GetComponent<Component::ParticleEmitter>()->particleType.lifeTime = 3600.f;
    batman->GetComponent<Component::ParticleEmitter>()->particleType.velocity *= 1.f;
    batman->AddComponent<Component::CollisionSphere>()->radius = 1.f;
    Component::PointLight* batLight = batman->AddComponent<Component::PointLight>();
    batLight->diffuse = Vector3(0.f, 1.f, 0.f);

    Entity* terrain = scene.CreateEntity();
    terrain->AddComponent<Component::Transform>()->position = Vector3(0.f, -10.f, 0.f);
    terrain->GetComponent<Component::Transform>()->scale *= 20.f;
    terrain->AddComponent<Component::Model>()->geometry = resourceManager->CreatePlane();
    terrain->GetComponent<Component::Model>()->diffuse = resourceManager->Create2DTexture(L"Resources/Assets/TerrainDiffuse.png");
    terrain->GetComponent<Component::Model>()->dispacement = resourceManager->Create2DTexture(L"Resources/Assets/TerrainDisplacement.png");
    terrain->GetComponent<Component::Model>()->bump = resourceManager->Create2DTexture(L"Resources/Assets/TerrainBump.png");
    terrain->GetComponent<Component::Model>()->LOD = 40.f;
    terrain->GetComponent<Component::Model>()->displacementAmplitude = 0.2f;
    //terrain->GetComponent<Component::Model>()->viewFrustumCull = false;

    Entity* skydome = scene.CreateEntity();
    skydome->AddComponent<Component::Transform>()->scale *= 20.f;
    skydome->AddComponent<Component::Model>()->geometry = resourceManager->CreateOBJMesh("Resources/Assets/Skydome/Skydome.obj");
    skydome->GetComponent<Component::Model>()->diffuse = resourceManager->Create2DTexture(L"Resources/Assets/Skydome/Skydome.jpg");
    //skydome->GetComponent<Component::Model>()->viewFrustumCull = false;
    ///skydome->GetComponent<Component::Model>()->SetMaterial(resourceManager->CreateMaterial<Material::SceneryMaterial>("sky"));
    Entity* grounddome = scene.CreateEntity();
    grounddome->AddComponent<Component::Transform>()->Pitch(180.f);
    grounddome->GetComponent<Component::Transform>()->Yaw(180.f);
    grounddome->GetComponent<Component::Transform>()->scale *= 20.f;
    grounddome->AddComponent<Component::Model>()->geometry = resourceManager->CreateOBJMesh("Resources/Assets/Skydome/Skydome.obj");
    grounddome->GetComponent<Component::Model>()->diffuse = resourceManager->Create2DTexture(L"Resources/Assets/Skydome/Skydome.jpg");
    //grounddome->GetComponent<Component::Model>()->viewFrustumCull = false;
    ///grounddome->GetComponent<Component::Model>()->SetMaterial(resourceManager->CreateMaterial<Material::SceneryMaterial>("sky"));

    int size = 2;
    int space = 3;
    for (int x = -size; x <= size; x++) {
        for (int y = -size; y <= size; y++) {
            for (int z = -size; z <= size; z++) {
                Entity* entity = scene.CreateEntity();
                entity->AddComponent<Component::Transform>()->position = Vector3(x, y, z) * space;
                entity->AddComponent<Component::Model>();
                Component::PointLight* light = entity->AddComponent<Component::PointLight>();
                light->diffuse = Vector3(std::abs(x) % 2, std::abs(y) % 3, std::abs(z) % 4);
                light->intensity = 2.f;
                light->radius = 4.f;
                entity->AddComponent<Component::Physics>()->angularVelocity = Vector3(std::abs(x) % 2, std::abs(y) % 3, std::abs(z) % 4) * 0.1f;
            }
        }
    }

    GravityCenter* centerOfGravity = new GravityCenter(&scene, &engine);

    Camera* batcam = new Camera(&scene, &engine);
    Image* frameBuffer = resourceManager->CreateImage(window->GetWidth(), window->GetHeight());
    Vector2 uvOffset = Vector2(0.f, 0.f);

    // Profiling variables
    std::string slowestSystem = "None";
    float slowestTime = 0.f;
    std::chrono::steady_clock::time_point systemTime;
    std::chrono::steady_clock::time_point outputTime = std::chrono::steady_clock::now();
    float totalRunTime = 0.f;


    // Main game loop
    std::chrono::steady_clock::time_point lastTime = std::chrono::steady_clock::now();
    while (window->Running()) {
        float deltaTime = std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::steady_clock::now() - lastTime).count();
        lastTime = std::chrono::steady_clock::now();
        window->SetTitle(std::to_string(1 / deltaTime) + " fps : " + std::to_string(deltaTime * 1000.f) + " ms/frame");

        float tmpTime = 0.f;
        systemTime = std::chrono::steady_clock::now();
        systemManager->updateSystem->Update(scene);
        tmpTime = std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::steady_clock::now() - systemTime).count();
        if (tmpTime > slowestTime) {
            slowestTime = tmpTime;
            slowestSystem = "updateSystem";
        }
        systemTime = std::chrono::steady_clock::now();
        systemManager->particleSystem->Update(scene, deltaTime);
        tmpTime = std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::steady_clock::now() - systemTime).count();
        if (tmpTime > slowestTime) {
            slowestTime = tmpTime;
            slowestSystem = "particleSystem";
        }
        systemTime = std::chrono::steady_clock::now();
        systemManager->physicsSystem->Update(scene, deltaTime);
        tmpTime = std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::steady_clock::now() - systemTime).count();
        if (tmpTime > slowestTime) {
            slowestTime = tmpTime;
            slowestSystem = "physicsSystem";
        }

        // Update Game Logic
        Vector2 screenSpaceMousePos = inputManager->GetScreenSpaceMousePosition();
        Vector3 mouseRay = inputManager->GetMouseRay(batcam->node->GetComponent<Component::Lens>());
        centerOfGravity->node->GetComponent<Component::Transform>()->position = batcam->node->GetComponent<Component::Transform>()->position + mouseRay * 6.f;
        skydome->GetComponent<Component::Transform>()->position = batcam->node->GetComponent<Component::Transform>()->position;
        grounddome->GetComponent<Component::Transform>()->position = batcam->node->GetComponent<Component::Transform>()->position + Vector3(0.f, -2.5f, 0.f);
        
        // TMP PICKING
        std::vector<Component::CollisionSphere*> collisionVec = scene.Get<Component::CollisionSphere>();
        for (auto& collision : collisionVec) {
            if (collision->hit)
                collision->entity->GetComponent<Component::Transform>()->scale = Vector3(2, 2, 2) * 0.1f;
            else
                collision->entity->GetComponent<Component::Transform>()->scale = Vector3(1, 1, 1) * 0.1f;
        }


        systemTime = std::chrono::steady_clock::now();
        systemManager->transformSystem->Update(scene);
        tmpTime = std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::steady_clock::now() - systemTime).count();
        if (tmpTime > slowestTime) {
            slowestTime = tmpTime;
            slowestSystem = "TransformSystem";
        }

        systemTime = std::chrono::steady_clock::now();
        systemManager->pickingSystem->Update(scene, batcam->node->GetComponent<Component::Lens>());
        tmpTime = std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::steady_clock::now() - systemTime).count();
        if (tmpTime > slowestTime) {
            slowestTime = tmpTime;
            slowestSystem = "PickingSystem";
        }

        systemTime = std::chrono::steady_clock::now();
        systemManager->renderSystem->Update(scene);
        tmpTime = std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::steady_clock::now() - systemTime).count();
        if (tmpTime > slowestTime) {
            slowestTime = tmpTime;
            slowestSystem = "renderSystem";
        }
        systemTime = std::chrono::steady_clock::now();

        // FX
        uvOffset += DirectX::SimpleMath::Vector2(1.f, 1.f) * deltaTime * 0.05f;
        // TODO FIX particle artifact where you can see the quads, DEPTH_DESC?
        fxManager->Refraction(batcam->node->GetComponent<Component::Lens>()->image, frameBuffer, uvOffset, batcam->node->GetComponent<Component::Lens>());
        window->Present(frameBuffer);

        // TMP
        // TODO Compare COPYPS, COPYCS, COPYCPU // MAKE LOG to store info of console output stream in txt, IF COPYPS isn't fastest, change backbuffer
        // TODO Set up AntTweekbar to manipulate refraction
        
        if (std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::steady_clock::now() - outputTime).count() > 3.f) {
            outputTime = std::chrono::steady_clock::now();
            totalRunTime += 3.f;
            std::cout << slowestSystem << ": " << slowestTime * 1000.f << " ms " << static_cast<unsigned int>(slowestTime / deltaTime * 100.f) << "(%). Particles: " << scene.GetParticleAmount() << std::endl << "Total run time: " << totalRunTime << std::endl;
        }
        slowestTime = 0.f;
    }

    delete batcam;
    delete centerOfGravity;
    return 0;
}

// --- GRAVITYCENTER ---
GravityCenter::GravityCenter(Scene* scene, E11::Device* engine) {
    node = scene->CreateEntity();
    node->AddComponent<Component::Transform>()->position = Vector3(0.f, 0.f, 5.f);
    node->AddComponent<Component::ParticleGravityCenter>()->gravityCoefficient *= 3.f;
    node->AddComponent<Component::Model>();
    node->GetComponent<Component::Transform>()->scale *= 0.1f;
    node->AddComponent<Component::Update>()->updateFunction = std::bind(&GravityCenter::mUpdate, *this);
    node->AddComponent<Component::Physics>()->drag = 10.f;
    Component::PointLight* light = node->AddComponent<Component::PointLight>();
    light->diffuse = Vector3(0.f, 1.f, 0.f);
    light->radius = 3.f / node->GetComponent<Component::Transform>()->GetMaxScale();
    light->intensity = 10.f;
}

GravityCenter::~GravityCenter() {
}

void GravityCenter::mUpdate() {
    float speed = 0.1f;
    if (GetAsyncKeyState(_In_ 'O')) {
        node->GetComponent<Component::Physics>()->velocity.z += speed;
    }
    if (GetAsyncKeyState(_In_ 'L')) {
        node->GetComponent<Component::Physics>()->velocity.z -= speed;
    }

    if (GetAsyncKeyState(_In_ 'F')) {
        node->GetComponent<Component::ParticleGravityCenter>()->gravityCoefficient = 30.f;
    }
    else if (GetAsyncKeyState(_In_ 'G')) {
        node->GetComponent<Component::ParticleGravityCenter>()->gravityCoefficient = 1.f;
    }
    else {
        node->GetComponent<Component::ParticleGravityCenter>()->gravityCoefficient = 0.f;
    }
}

// --- CAMERA ---
Camera::Camera(Scene* scene, E11::Device* engine) {
    node = scene->CreateEntity();
    node->AddComponent<Component::Transform>()->position.z = 0.f;
    node->AddComponent<Component::Lens>()->image = engine->GetResourceManager()->CreateImage(engine->GetWindow()->GetWidth(), engine->GetWindow()->GetHeight());
    node->AddComponent<Component::Update>()->updateFunction = std::bind(&Camera::mUpdate, *this);
    node->AddComponent<Component::Physics>()->drag = 10.f;
    node->GetComponent<Component::Physics>()->angularDragFactor = 2.f;
}

Camera::~Camera() {
}

void Camera::mUpdate() {
    float speed = 2.f;
    if (GetAsyncKeyState(_In_ 'W')) {
        node->GetComponent<Component::Physics>()->velocity = node->GetComponent<Component::Transform>()->frontDirection * speed;
    }
    if (GetAsyncKeyState(_In_ 'S')) {
        node->GetComponent<Component::Physics>()->velocity = node->GetComponent<Component::Transform>()->frontDirection * -speed;
    }
    if (GetAsyncKeyState(_In_ 'A')) {
        node->GetComponent<Component::Physics>()->velocity = node->GetComponent<Component::Transform>()->sideDirection * -speed;
    }
    if (GetAsyncKeyState(_In_ 'D')) {
        node->GetComponent<Component::Physics>()->velocity = node->GetComponent<Component::Transform>()->sideDirection * speed;
    }
    if (GetAsyncKeyState(_In_ 'Q')) {
        node->GetComponent<Component::Physics>()->velocity = node->GetComponent<Component::Transform>()->upDirection * speed;
    }
    if (GetAsyncKeyState(_In_ 'E')) {
        node->GetComponent<Component::Physics>()->velocity = node->GetComponent<Component::Transform>()->upDirection * -speed;
    }

    float rotationSpeed = 0.1f;
    if (GetAsyncKeyState(_In_ 'Z')) {
        node->GetComponent<Component::Physics>()->angularVelocity.x = rotationSpeed;
    }
    if (GetAsyncKeyState(_In_ 'X')) {
        node->GetComponent<Component::Physics>()->angularVelocity.x = -rotationSpeed;
    }
}

