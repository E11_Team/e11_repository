#pragma once

#include <random>

struct ID3D11Device;
struct ID3D11DeviceContext;
struct IDXGISwapChain;

namespace E11 {
    class Window;
    class ResourceManager;
    class SystemManager;
    class InputManager;
    class FXManager;
    class CoreManager {
    public:
        bool Init(unsigned int winWidth, unsigned int winHeight);
        static CoreManager& GetInstance();
        ID3D11Device* device;
        ID3D11DeviceContext* deviceContext;
        IDXGISwapChain* swapChain;
        Window* window;
        ResourceManager* resourceManager;
        SystemManager* systemManager;
        InputManager* inputManager;
        FXManager* fxManager;
        std::random_device randomDevice;
        std::mt19937 randomNumberGenerator;
    private:
        CoreManager();
        ~CoreManager();
    };
    CoreManager& Core();
}
