#include "Core.hpp"

#include "Window.hpp"
#include "Managers/ResourceManager.hpp"
#include "Managers/SystemManager.hpp"
#include "Managers/InputManager.hpp"
#include "Managers/FXManager.hpp"

using namespace E11;

bool CoreManager::Init(unsigned int winWidth, unsigned int winHeight) {
    window = new Window(winWidth, winHeight);
    resourceManager = new ResourceManager();
    systemManager = new SystemManager();
    inputManager = new InputManager();
    fxManager = new FXManager();
    return true;
}

CoreManager::CoreManager() {
    window = nullptr;
    resourceManager = nullptr;
    systemManager = nullptr;
    inputManager = nullptr;
    fxManager = nullptr;
    randomNumberGenerator.seed(randomDevice());
}

CoreManager::~CoreManager() {
    device->Release();
    deviceContext->Release();
    swapChain->Release();
    delete window;
    delete resourceManager;
    delete systemManager;
    delete inputManager;
    delete fxManager;
}

CoreManager& CoreManager::GetInstance() {
    static CoreManager instance;
    return instance;
}

CoreManager& E11::Core() {
    return CoreManager::GetInstance();
}
