#include "Texture2D.hpp"

#include <d3d11.h>
#include <WICTextureLoader.h>

#include "../Core.hpp"

using namespace E11;
using namespace Texture;

Texture2D::Texture2D(std::wstring filePath) : SuperTexture() {
    DirectX::CreateWICTextureFromFile(Core().device, filePath.c_str(), NULL, &srv);
    ID3D11Resource* res;
    srv->GetResource(&res);
    ID3D11Texture2D* texture2d;
    res->QueryInterface(&texture2d);
    D3D11_TEXTURE2D_DESC desc;
    texture2d->GetDesc(&desc);
    mWidth = desc.Width;
    mHeight = desc.Height;
    texture2d->Release();
    res->Release();
}

Texture2D::~Texture2D() {
}

unsigned int Texture2D::GetWidth() const {
    return mWidth;
}

unsigned int Texture2D::GetHeight() const {
    return mHeight;
}

