#pragma once

struct ID3D11ShaderResourceView;

namespace E11 {
    namespace Texture {
        class SuperTexture {
        public:
            SuperTexture();
            virtual ~SuperTexture();
            virtual unsigned int GetWidth() const = 0;
            virtual unsigned int GetHeight() const = 0;
            ID3D11ShaderResourceView* GetSRV();
        protected:
            ID3D11ShaderResourceView* srv;
        };
    }
}