#include "SuperTexture.hpp"

#include <d3d11.h>

using namespace E11;
using namespace Texture;

SuperTexture::SuperTexture() {
    srv = nullptr;
}

SuperTexture::~SuperTexture() {
    srv->Release();
}

ID3D11ShaderResourceView* SuperTexture::GetSRV() {
    return srv;
}
