#pragma once

#include "SuperTexture.hpp"

#include <string>

namespace E11 {
    namespace Texture {
        class Texture2D : public SuperTexture{
        public:
            Texture2D(std::wstring filePath);
            virtual ~Texture2D();
            unsigned int GetWidth() const;
            unsigned int GetHeight() const;
        private:
            unsigned int mWidth;
            unsigned int mHeight;
        };
    }
}