#pragma once

struct ID3D11Device;
struct ID3D11DeviceContext;
struct IDXGISwapChain;
struct ID3D11RenderTargetView;
struct ID3D11ShaderResourceView;

#include <Windows.h>
#include <string>
#include <SimpleMath.h>

static LRESULT CALLBACK WindowProcedure(HWND handle, UINT message, WPARAM wParam, LPARAM lParam);

namespace E11 {
    class Image;
    class Window {
    public:
        Window(unsigned int width, unsigned int height, bool fullscreen = false, bool borderless = false, std::string title = "");
        ~Window();
        HWND windowHandle;
        ID3D11RenderTargetView* rtv;
        unsigned int GetWidth();
        unsigned int GetHeight();
        //float GetNearZ();
        //float GetFarZ();
        void SetTitle(std::string title);
        bool Running() const;
        void Close();
        void Present(Image* srcImage);
    private:
        unsigned int mWidth, mHeight;
        //float mNearZ, mFarZ;
    };
}
