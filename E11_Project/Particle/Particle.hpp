#pragma once

#include <SimpleMath.h>

namespace E11 {
    struct Particle {
        DirectX::SimpleMath::Vector3 position = DirectX::SimpleMath::Vector3(0.f, 0.f, 0.f);
        DirectX::SimpleMath::Vector2 scale = DirectX::SimpleMath::Vector2(1.f, 1.f);
        DirectX::SimpleMath::Vector3 velocity = DirectX::SimpleMath::Vector3(0.f, 1.f, 0.f);
        float maxVelocity = 20.f;
        DirectX::SimpleMath::Vector3 acceleration = DirectX::SimpleMath::Vector3(0.f, 0.f, 0.f);
        float lifeLenght = 10.f;
        float elapsedTime = 0.f;
        float drag = 0.1f;
        bool cull = false;
    };
    struct rawParticle {
        DirectX::SimpleMath::Vector3 position;
        DirectX::SimpleMath::Vector2 scale;
    };
}