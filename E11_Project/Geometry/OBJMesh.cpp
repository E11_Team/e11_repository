#include "OBJMesh.hpp"

#include <d3d11.h>
#include <fstream>
#include <vector>

#include <SimpleMath.h>

#include "../Core.hpp"

using namespace E11;
using namespace Geometry;

using namespace DirectX;
using namespace SimpleMath;

OBJMesh::OBJMesh(std::string filePath) : SuperGeometry() {
    std::ifstream file(filePath);

    if (file) {
        std::vector<Vector3> posVec;
        std::vector<Vector3> normVec;
        std::vector<Vector2> uvVec;
        //std::vector<Vector3> tangentVec;
        struct VertP {
            unsigned int posIndex, uvIndex, normIndex;
        };
        std::vector<VertP> vertPVec;

        while (!file.eof()) {
            char c1, c2;
            Vector3 vec3;
            Vector2 vec2;
            //Vector3 edge1;
            //Vector3 edge2;
            //Vector2 deltaUV1;
            //Vector2 deltaUV2;
            //Vector3 tangent;
            file >> c1;
            switch (c1) {
            case 'v':
                file >> std::noskipws >> c2 >> std::skipws;
                switch (c2) {
                case ' ':
                    file >> vec3.x >> vec3.y >> vec3.z;
                    posVec.push_back(vec3);
                    //tangentVec.push_back(Vector3(0.f, 0.f, 0.f));
                    break;
                case 'n':
                    file >> vec3.x >> vec3.y >> vec3.z;
                    normVec.push_back(vec3);
                    break;
                case 't':
                    file >> vec2.x >> vec2.y;
                    uvVec.push_back(vec2);
                    break;
                }
                break;
            case 'f':
                VertP vertP[3];
                //float f;
                for (unsigned int i = 0; i < 3; i++) {
                    file >> vertP[i].posIndex >> c1 >> vertP[i].uvIndex >> c2 >> vertP[i].normIndex;
                    vertP[i].posIndex -= 1;
                    vertP[i].uvIndex -= 1;
                    vertP[i].normIndex -= 1;
                }
                // Calculate tangent
                /*edge1 = posVec[vertP[1].posIndex] - posVec[vertP[0].posIndex];
                edge2 = posVec[vertP[2].posIndex] - posVec[vertP[0].posIndex];

                deltaUV1 = uvVec[vertP[1].uvIndex] - uvVec[vertP[0].uvIndex];
                deltaUV2 = uvVec[vertP[2].uvIndex] - uvVec[vertP[0].uvIndex];

                f = 1.f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

                tangent = f * (deltaUV2.y * edge1 - deltaUV1.y * edge2);
                tangent.Normalize();

                tangentVec[vertP[0].posIndex] = tangent;
                tangentVec[vertP[1].posIndex] = tangent;
                tangentVec[vertP[2].posIndex] = tangent;
                */

                vertPVec.push_back(vertP[0]);
                vertPVec.push_back(vertP[1]);
                vertPVec.push_back(vertP[2]);

                break;
            default:
                file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
        }

        file.close();

        nrOfVertices = static_cast<unsigned int>(vertPVec.size());
        Vertex* vertices = new Vertex[nrOfVertices];

        for (unsigned int i = 0; i < nrOfVertices; ++i) {
            Vertex vertex;
            vertex.position = posVec[vertPVec[i].posIndex];
            vertex.normal = normVec[vertPVec[i].normIndex];
            //vertex.tangent = tangentVec[vertPVec[i].posIndex];
            //vertex.binormal = vertex.normal.Cross(vertex.tangent);
            vertex.uv = uvVec[vertPVec[i].uvIndex];
            vertices[i] = vertex;
            float length = vertex.position.x*vertex.position.x + vertex.position.y*vertex.position.y + vertex.position.z*vertex.position.z;
            if (length > radius)
                radius = length;
        }
        radius = std::sqrtf(radius);

        // Fill out the buffer description to use when creating our vertex buffer.
        D3D11_BUFFER_DESC bufferDesc;
        ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
        bufferDesc.ByteWidth = sizeof(Vertex) * nrOfVertices;// The buffer needs to know the total size of its data, i.e. all vertices.
        bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;			 // A buffer whose contents never change after creation is IMMUTABLE.
        bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;	 // For a vertex buffer, this flag must be specified.
        bufferDesc.CPUAccessFlags = 0;						 // The CPU won't access the buffer after creation.
        bufferDesc.MiscFlags = 0;							 // The buffer is not doing anything extraordinary.
        bufferDesc.StructureByteStride = 0;					 // Only used for structured buffers, which a vertex buffer is not.

                                                             // Define what data our buffer will contain.
        D3D11_SUBRESOURCE_DATA bufferContents;
        bufferContents.pSysMem = vertices;

        // Create the buffer.
        Core().device->CreateBuffer(&bufferDesc, &bufferContents, &vertexBuffer);

        delete vertices;
        posVec.clear();
        normVec.clear();
        uvVec.clear();
        //tangentVec.clear();
        vertPVec.clear();
    }
}

OBJMesh::~OBJMesh() {
}
