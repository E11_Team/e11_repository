#include "SuperGeometry.hpp"

#include <d3d11.h>

using namespace E11;
using namespace Geometry;

SuperGeometry::SuperGeometry() {
    vertexBuffer = nullptr;
    nrOfVertices = 0;
    radius = 0.f;
}

SuperGeometry::~SuperGeometry() {
    if (vertexBuffer != nullptr)
        vertexBuffer->Release();
}
