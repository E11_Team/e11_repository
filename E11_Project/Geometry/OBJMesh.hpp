#pragma once

#define NOMINMAX

#include "SuperGeometry.hpp"

#include <string>

namespace E11 {
    namespace Geometry {
        class OBJMesh : public SuperGeometry {
        public:
            OBJMesh(std::string filePath);
            ~OBJMesh();
        };
    }
}