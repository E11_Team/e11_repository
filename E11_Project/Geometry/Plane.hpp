#pragma once

#include "SuperGeometry.hpp"

namespace E11 {
    namespace Geometry {
        class Plane : public SuperGeometry {
        public:
            Plane();
            ~Plane();
        };
    }
}