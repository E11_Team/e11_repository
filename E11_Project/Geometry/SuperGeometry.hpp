#pragma once

#include <SimpleMath.h>

struct ID3D11Buffer;

namespace E11 {
    namespace Geometry {
        struct Vertex {
            DirectX::SimpleMath::Vector3 position;
            DirectX::SimpleMath::Vector3 normal;
            //DirectX::SimpleMath::Vector3 tangent;
            //DirectX::SimpleMath::Vector3 binormal;
            DirectX::SimpleMath::Vector2 uv;
        };
        // http://stackoverflow.com/questions/5255806/how-to-calculate-tangent-and-binormal
        class SuperGeometry {
        public:
            SuperGeometry();
            virtual ~SuperGeometry();
            ID3D11Buffer* vertexBuffer;
            unsigned int nrOfVertices;
            // Radius of sphere containing the mesh
            float radius;
        };
    }
}