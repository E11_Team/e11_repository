#include "Plane.hpp"

#include <d3d11.h>

#include "../Core.hpp"

using namespace E11;
using namespace Geometry;

Plane::Plane() : SuperGeometry() {
    Vertex vertices[] =
    {
        { DirectX::XMFLOAT3(-0.5f, 0.0f, 0.5f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f), DirectX::XMFLOAT2(0.f, 0.f) },
        { DirectX::XMFLOAT3(0.5f, 0.0f, -0.5f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f), DirectX::XMFLOAT2(1.f, 1.f) },
        { DirectX::XMFLOAT3(-0.5f, 0.0f, -0.5f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f), DirectX::XMFLOAT2(0.f, 1.f) },

        { DirectX::XMFLOAT3(0.5f, 0.f, 0.5f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f), DirectX::XMFLOAT2(1.f, 0.f) },
        { DirectX::XMFLOAT3(0.5f, 0.0f, -0.5f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f), DirectX::XMFLOAT2(1.f, 1.f) },
        { DirectX::XMFLOAT3(-0.5f, 0.0f, 0.5f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f), DirectX::XMFLOAT2(0.f, 0.f) }
    };
    // Fill out the buffer description to use when creating our vertex buffer.
    D3D11_BUFFER_DESC bufferDesc;
    ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
    bufferDesc.ByteWidth = sizeof(vertices);			// The buffer needs to know the total size of its data, i.e. all vertices.
    bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;			// A buffer whose contents never change after creation is IMMUTABLE.
    bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;	// For a vertex buffer, this flag must be specified.
    bufferDesc.CPUAccessFlags = 0;						// The CPU won't access the buffer after creation.
    bufferDesc.MiscFlags = 0;							// The buffer is not doing anything extraordinary.
    bufferDesc.StructureByteStride = 0;					// Only used for structured buffers, which a vertex buffer is not.

                                                        // Define what data our buffer will contain.
    D3D11_SUBRESOURCE_DATA bufferContents;
    bufferContents.pSysMem = vertices;

    // Create the buffer.
    Core().device->CreateBuffer(&bufferDesc, &bufferContents, &vertexBuffer);

    nrOfVertices = 6;

    radius = sqrtf(0.5f*0.5f+0.5f*0.5f+0.5f*0.5f);
}

Plane::~Plane() {
}
