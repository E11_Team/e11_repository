#pragma once

template <typename T>
class DynamicArray {
public:
    DynamicArray(const unsigned int capacity);
    ~DynamicArray();
    T* Push(const T data);
    T Pop();
    T* At(const unsigned int index);
    void SetAt(const T data, const unsigned int index);
    const unsigned int Size();
    const unsigned int Capacity();
    T* GetArrPointer();
private:
    T* mArr;
    unsigned int mCapacity;
    unsigned int mNrOfElements;
};

template <typename T>
DynamicArray<T>::DynamicArray(const unsigned int capacity) {
    mArr = new T[capacity];
    mCapacity = capacity;
    mNrOfElements = 0;
}

template <typename T>
DynamicArray<T>::~DynamicArray() {
    delete[] mArr;
}

template <typename T>
T* DynamicArray<T>::Push(const T data) {
    if (mNrOfElements < mCapacity) {
        mArr[mNrOfElements] = data;
        return &mArr[mNrOfElements++];
    }
    return nullptr;
}

template <typename T>
T DynamicArray<T>::Pop() {
    if (mNrOfElements > 0)
        return mArr[--mNrOfElements];
    return T();
}

template <typename T>
T* DynamicArray<T>::At(const unsigned int index) {
    if (index < mNrOfElements)
        return &mArr[index];
    return nullptr;
}

template <typename T>
void DynamicArray<T>::SetAt(const T data, const unsigned int index) {
    if (index < mNrOfElements)
        mArr[index] = data;
}

template <typename T>
const unsigned int DynamicArray<T>::Size() {
    return mNrOfElements;
}

template <typename T>
const unsigned int DynamicArray<T>::Capacity() {
    return mCapacity;
}

template <typename T>
T* DynamicArray<T>::GetArrPointer() {
    return mArr;
}
