#pragma once

#include <map>
#include <typeinfo>

#include "../Component/SuperComponent.hpp"

namespace E11 {
    class Scene;
    class Entity {
    public:
        Entity(Scene* scene);
        ~Entity();
        template<typename T> T* AddComponent();
        template<typename T> T* GetComponent();
    private:
        Scene* mScene;
        std::map<const std::type_info*, Component::SuperComponent*> mComponents;
    };

    template <typename T> T* Entity::AddComponent() {
        const std::type_info* componentType = &typeid(T*);
        if (mComponents.find(componentType) != mComponents.end())
            return nullptr;
        T* component = new T(this);
        mComponents[componentType] = component;
        mScene->AddComponentToList(component, componentType);
        return component;
    }

    template <typename T> T* Entity::GetComponent() {
        if (this->mComponents.count(&typeid(T*)) != 0)
            return static_cast<T*>(this->mComponents[&typeid(T*)]);
        return nullptr;
    }
}
