#pragma once

struct ID3D11ComputeShader;
struct ID3D11UnorderedAccessView;
struct ID3D11ShaderResourceView;
struct ID3D11Buffer;
struct ID3D11SamplerState;

#include <string>
#include <functional>

namespace E11 {
    namespace Texture {
        class Texture2D;
    }
    class ComputeShader {
    public:
        ComputeShader(std::wstring shaderPath);
        ~ComputeShader();
        ID3D11ComputeShader* shader;
        Texture::Texture2D* textures[7];
        unsigned int nrOfTextures;
        ID3D11Buffer* constBuffers[8];
        unsigned int nrOfConstBuffers;
        std::function<void()> updateFunction;
        ID3D11SamplerState* samplerStates[2];
        void Dispatch(unsigned int x, unsigned int y, ID3D11ShaderResourceView* srcSrv, ID3D11UnorderedAccessView* trgUav);
    private:
        void mDummyFunction();
    };
}
