#include "ComputeShader.hpp"

#include <d3d11.h>
#include <d3dcompiler.h>

#include "../Core.hpp"
#include "../Texture/Texture2D.hpp"

using namespace E11;

ComputeShader::ComputeShader(std::wstring shaderPath) {
    nrOfTextures = 0;
    nrOfConstBuffers = 0;

    D3D11_SAMPLER_DESC sampDesc;
    /*
    desc.Filter = D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT; // bilinear
    desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR; // trilinear
    desc.Filter = D3D11_FILTER_ANISOTROPIC; // anisotropic
    */
    ZeroMemory(&sampDesc, sizeof(D3D11_SAMPLER_DESC));
    sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.MipLODBias = 0.f;
    sampDesc.MaxAnisotropy = 1;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
    sampDesc.BorderColor[0] = 0.f;
    sampDesc.BorderColor[1] = 0.f;
    sampDesc.BorderColor[2] = 0.f;
    sampDesc.BorderColor[3] = 0.f;
    sampDesc.MinLOD = 0.f;
    sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
    Core().device->CreateSamplerState(&sampDesc, &samplerStates[0]);

    sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
    sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
    Core().device->CreateSamplerState(&sampDesc, &samplerStates[1]);

    updateFunction = std::bind(&ComputeShader::mDummyFunction, this);

    ID3DBlob* compiledCS = nullptr;
    ID3DBlob* errorBlob = nullptr;
    HRESULT hr = D3DCompileFromFile(
        shaderPath.c_str(),		// The path to the shader file relative to the .vxproj folder.
        nullptr,		// We don't use any defines.
        nullptr,		// We don't have any includes.
        "main",			// The name of the entry function. Must match function in source data.
        "cs_5_0",		// The shader model to use, "cs" specifies it is a compute shader, 5_0 that it is shader model 5.0.
        0,				// No shader compile options.
        0,				// Ignored when compiling a shader (effect compile options).
        &compiledCS,	// [out] Compiled shader data.
        &errorBlob			// [out] Compile time error data.
        );

    if (FAILED(hr)) {
        std::string errorMsg = (char*)errorBlob->GetBufferPointer();
        OutputDebugStringA(errorMsg.c_str());
        errorBlob->Release();
    }

    Core().device->CreateComputeShader(
        compiledCS->GetBufferPointer(),
        compiledCS->GetBufferSize(),
        NULL,
        &shader
        );
    compiledCS->Release();
}

ComputeShader::~ComputeShader() {
    if (shader != nullptr)
        shader->Release();
    for (unsigned int i = 0; i < nrOfConstBuffers; ++i)
        constBuffers[i]->Release();
    samplerStates[0]->Release();
    samplerStates[1]->Release();
}

void ComputeShader::Dispatch(unsigned int x, unsigned int y, ID3D11ShaderResourceView* srcSrv, ID3D11UnorderedAccessView* trgUav) {
    // Update resources
    updateFunction();
    
    // Bind resources
    Core().deviceContext->CSSetSamplers(0, 2, samplerStates);
    Core().deviceContext->CSSetShader(shader, NULL, NULL);
    Core().deviceContext->CSSetShaderResources(0, 1, &srcSrv);
    for (unsigned int i = 0; i < nrOfTextures; ++i) {
        ID3D11ShaderResourceView* srv = textures[i]->GetSRV();
        Core().deviceContext->CSSetShaderResources(i+1, 1, &srv);
    }
    Core().deviceContext->CSSetUnorderedAccessViews(0, 1, &trgUav, 0);
    Core().deviceContext->CSSetConstantBuffers(0, nrOfConstBuffers, constBuffers);

    // Dispatch
    Core().deviceContext->Dispatch(x, y, 1);

    // Unbind resources
    ID3D11SamplerState *const pSS[2] = { NULL, NULL };
    Core().deviceContext->CSSetSamplers(0, 2, pSS);
    Core().deviceContext->CSSetShader(NULL, NULL, NULL);
    ID3D11ShaderResourceView *const pSRV[1] = { NULL };
    Core().deviceContext->CSSetShaderResources(0, 1, pSRV);
    for (unsigned int i = 0; i < nrOfTextures; ++i)
        Core().deviceContext->CSSetShaderResources(i + 1, 1, pSRV);
    ID3D11UnorderedAccessView *const pUAV[1] = { NULL };
    Core().deviceContext->CSSetUnorderedAccessViews(0, 1, pUAV, 0);
    ID3D11Buffer* pBuff[1] = { NULL };
    for (unsigned int i = 0; i < nrOfConstBuffers; ++i)
        Core().deviceContext->CSGetConstantBuffers(i + 1, 1, pBuff);
}

void ComputeShader::mDummyFunction() {
}

