#include "Scene.hpp"

#include "../Entity/Entity.hpp"
#include "../Particle/Particle.hpp"

#include "../Core.hpp"

#include <d3d11.h>

using namespace E11;

Scene::Scene(unsigned int particleCap) {
    mParticleArr = new DynamicArray<Particle>(particleCap);

    // Particle Vertex Buffer
    D3D11_BUFFER_DESC buffDesc;
    ZeroMemory(&buffDesc, sizeof(D3D11_BUFFER_DESC));
    buffDesc.ByteWidth = sizeof(rawParticle) * particleCap;
    buffDesc.Usage = D3D11_USAGE_DYNAMIC;
    buffDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    buffDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    buffDesc.MiscFlags = 0;
    buffDesc.StructureByteStride = 0;
    Core().device->CreateBuffer(&buffDesc, NULL, &mParticleVertexBuffer);

    //http://gamedev.stackexchange.com/questions/71014/passing-input-to-compute-shader
    //http://d.hatena.ne.jp/hanecci/20111126/1322332593
    // Particle Source Buffer
    ZeroMemory(&buffDesc, sizeof(D3D11_BUFFER_DESC));
    buffDesc.ByteWidth = sizeof(Particle) * particleCap;
    buffDesc.Usage = D3D11_USAGE_DYNAMIC;
    buffDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    buffDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    buffDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
    buffDesc.StructureByteStride = sizeof(Particle);
    Core().device->CreateBuffer(&buffDesc, NULL, &mParticleSourceBuffer);

    // Particle Target Buffer
    ZeroMemory(&buffDesc, sizeof(D3D11_BUFFER_DESC));
    buffDesc.ByteWidth = sizeof(Particle) * particleCap;
    buffDesc.Usage = D3D11_USAGE_DEFAULT;
    buffDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
    buffDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
    buffDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
    buffDesc.StructureByteStride = sizeof(Particle);
    Core().device->CreateBuffer(&buffDesc, NULL, &mParticleTargetBuffer);

    // Particle Source SRV
    D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
    ZeroMemory(&srvDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
    srvDesc.Format = DXGI_FORMAT_UNKNOWN;
    srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
    srvDesc.Buffer.ElementOffset = 0;
    srvDesc.Buffer.NumElements = particleCap;
    Core().device->CreateShaderResourceView(mParticleSourceBuffer, &srvDesc, &mParticleSourceSRV);

    // Particle Target UAV
    D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
    ZeroMemory(&uavDesc, sizeof(D3D11_UNORDERED_ACCESS_VIEW_DESC));
    uavDesc.Format = DXGI_FORMAT_UNKNOWN;
    uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
    uavDesc.Buffer.FirstElement = 0;
    uavDesc.Buffer.NumElements = particleCap;
    uavDesc.Buffer.Flags = 0;
    Core().device->CreateUnorderedAccessView(mParticleTargetBuffer, &uavDesc, &mParticleTargetUAV);
}

Scene::~Scene() {
    for (auto& entity : mEntities)
        delete entity;
    mEntities.clear();

    for (auto& it : mComponents) {
        for (Component::SuperComponent* component : it.second)
            delete component;
    }
    mComponents.clear();

    mParticleVertexBuffer->Release();
    mParticleSourceBuffer->Release();
    mParticleTargetBuffer->Release();
    mParticleSourceSRV->Release();
    mParticleTargetUAV->Release();
    delete mParticleArr;
}

Entity* Scene::CreateEntity() {
    Entity* entity = new Entity(this);
    mEntities.push_back(entity);
    return entity;
}

unsigned int Scene::GetParticleCap() const {
    return mParticleArr->Capacity();
}

unsigned int Scene::GetParticleAmount() const {
    return mParticleArr->Size();
}

void Scene::AddComponentToList(Component::SuperComponent* component, const std::type_info* componentType) {
    mComponents[componentType].push_back(component);
}

ID3D11Buffer* Scene::GetParticleVertexBuffer() {
    return mParticleVertexBuffer;
}

ID3D11Buffer* Scene::GetParticleSourceBuffer() {
    return mParticleSourceBuffer;
}

ID3D11Buffer* Scene::GetParticleTargetBuffer() {
    return mParticleTargetBuffer;
}

ID3D11ShaderResourceView* Scene::GetParticleSourceSRV() {
    return mParticleSourceSRV;
}

ID3D11UnorderedAccessView* Scene::GetParticleTargetUAV() {
    return mParticleTargetUAV;
}

DynamicArray<Particle>* Scene::GetParticleArr() {
    return mParticleArr;
}
