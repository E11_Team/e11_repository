#pragma once

#include <map>
#include <vector>
#include <typeinfo>

#include "../DynamicArray.hpp"

struct ID3D11Buffer;
struct ID3D11UnorderedAccessView;
struct ID3D11ShaderResourceView;

namespace E11 {
    class Entity;
    struct Particle;
    namespace Component {
        class SuperComponent;
    }
    class Scene {
        friend class Entity;
    public:
        Scene(unsigned int particleCap = 300000);
        ~Scene();
        Entity* CreateEntity();
        template <typename T> std::vector<T*>& Get();
        template <typename T> std::vector<T*>* GetPointer();
        unsigned int GetParticleCap() const;
        unsigned int GetParticleAmount() const;
        ID3D11Buffer* GetParticleVertexBuffer();
        ID3D11Buffer* GetParticleSourceBuffer();
        ID3D11Buffer* GetParticleTargetBuffer();
        ID3D11ShaderResourceView* GetParticleSourceSRV();
        ID3D11UnorderedAccessView* GetParticleTargetUAV();
        DynamicArray<Particle>* GetParticleArr();
    private:
        void AddComponentToList(Component::SuperComponent* component, const std::type_info* componentType);
        std::vector<Entity*> mEntities;
        std::map<const std::type_info*, std::vector<Component::SuperComponent*>> mComponents;
        ID3D11Buffer* mParticleVertexBuffer;
        ID3D11Buffer* mParticleSourceBuffer;
        ID3D11Buffer* mParticleTargetBuffer;
        ID3D11ShaderResourceView* mParticleSourceSRV;
        ID3D11UnorderedAccessView* mParticleTargetUAV;
        DynamicArray<Particle>* mParticleArr;
    };
    template <typename T> inline std::vector<T*>& Scene::Get() {
        return reinterpret_cast<std::vector<T*>&>(mComponents[&typeid(T*)]);
    }
    template <> inline std::vector<Entity*>* Scene::GetPointer() {
        return &mEntities;
    }
}
